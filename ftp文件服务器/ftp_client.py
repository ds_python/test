import time
from socket import *

ADDR = ('127.0.0.1',8888)

# 具体功能类存放在类里面
class FtpClient(object):
    def __init__(self, sockfd):
        self.sockfd = sockfd

    def do_get(self,filename):
        self.sockfd.send('G' + filename).encode()
        data = self.sockfd.recv(128).decode()
        if data == 'ok':
            fd = open(filename, 'wb')
            while True:
                data = self.sockfd.recv(1024)
                if data == b'##':
                    fd.write(data)
            fd.close()
        else:
            print(data)


    def do_put(self, filename):
        try:
            fd = open(filename,'wb')
            pass
        except IOError:
            print("err")
            self.sockfd.send('上传失败'.encode())
            return

        self.sockfd.send(b'ok')

        # 接收文件
        while True:
            data = self.sockfd.recv(1024)
            if data == b'##':
                break
            fd.write(data)
        fd.close()


    def do_list(self):
        """
        查看文件列表
        :param self:
        :return:
        """
        self.sockfd.send(b'L')
        # 等待回复
        print("等待服务器响应")

        data = self.sockfd.recv(128).decode()

        if data == 'ok':
            data = self.sockfd.recv(1024).decode()
            files = data.split(',')
            for file in files:
                print(file)  #打印文件名称
        else:
            pass


# 网络连接
def main():
    socket_fd = socket()
    try:
        socket_fd.connect(ADDR)
        pass
    except Exception as e:
        print(e)
        return

    ftp_client = FtpClient(socket_fd)  # 实例化客户端

    while True:
        print("\n ====== 命令选项 =====")
        print("*** list ***")
        print("*** get file ***")
        print("*** put file ***")
        print("*** quit     ***")
        print("****************")
        cmd = input("输入命令->")

        if cmd == 'list':
            ftp_client.do_list()
        elif cmd == 'quit':
            socket_fd.send('Q')
            socket_fd.close()
            sys.exit('谢谢使用')
        elif cmd[:3] == 'get':
            # 正则表达式
            filename = cmd.split(' ')[-1]
            ftp_client.do_get(filename)
        elif cmd[:3] == 'put':
            ftp_client.do_put(filename)



if __name__ == "__main__":
    main()

