from socket import *
import os, sys
import time
import signal

# 定义全局变量
HOST = '0.0.0.0'
PORT = 8888

ADDR = (HOST, PORT)
FILE_PATH = "~/Music/ftpFile/"

class ftpServer(object):
    def __init__(self,connfd):
        self.connd = connfd

    def do_get(self,filename):
        """
        打开文件
        :param filename:
        :return:
        """
        try:
            fd = open(FILE_PATH + filename,'fb')
        except IOError as e:
            print(e)
            self.connd.send('文件不存在'.encode)
            return

        self.connd.send(b'ok')
        time.sleep(0.1)

        # 发送文件内容
        while True:
            data = fd.read(1024)
            if not data:
                time.sleep(0.1)
                self.connd.send(b'##')

    def do_list(self):
        """
        获取文件库中的文件列表,（包含目录和隐藏文件）
        :return:
        """
        print("server do list")
        file_list = os.listdir(FILE_PATH)

        if not file_list:
            self.connd.send("文件列表为空".encode())
        else:
            self.connd.send(b'ok')
            time.sleep(0.1)

        files = ''
        for file in file_list:
            if file[0] != '.' and os.path.isfile(FILE_PATH + file):  # 判断是不是普通文件
                files += file + ','

        # 将拼接好的大字符串发送
        self.connd.send(files.encode())


    def do_put(self, filename):

        try:
            f = open(filename, 'rb')
        except IOError:
            print("没有找到该文件")
            return

        filename = filename.spit('/')[-1]  # 截取字符串取最后一个
        self.connd.send(("P "+filename).encode())
        data = self.connd.recv(128).decode()

        if data == 'ok':
            while True:
                data = f.read(1024)
                if not data:
                    time.sleep(0.1)
                    self.connd.send(b'##')
                    break
                self.connd.send(data)
            f.close()

        else:

            pass




# tcp套接字
def main():
    socket_fd = socket()
    socket_fd.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
    socket_fd.bind(ADDR)
    socket_fd.listen(3)
    print("Lisen the report 8888...")

    # 处理僵尸进程
    signal.signal(signal.SIGCHLD, signal.SIG_IGN)

    while True:
        try:
            connfd, addr = socket_fd.accept()
        except KeyboardInterrupt:
            socket_fd.close()
            sys.exit("服务器退出")
        except Exception as e:
            print("服务器异常:", e)
            continue
        print("连接客户端", addr)

    # 创建子进程
    pid = os.fork()
    if pid == 0:
        # 处理具体的客户端事物
        # socket_fd.close()
        ftp = FtpServer(connfd)
        print(ftp)

        while True:
            data = connfd.recv(1024).decode()

            try:
                # 可能会收到系统发送的空字符串
                if not data or data[0] == 'Q':
                    connfd.close()
                    break
                elif data[0] == 'L':
                    ftp.do_list()
                elif data[0] == 'G':
                    # 客户端需要下载文件了
                    filename = data.split(' ')[-1]
                    ftp.do_get()

            except Exception as e:
                print(e)

        print("处理具体事物")
        os._exit(0)
        #sys.exit("退出")
    else:
        print("111")
        pass


if __name__ == '__main__':
    main()

