import abc

from turtle import *
from datetime import *

"""
    尝试定义一个接口
"""


def skip(step):
    penup()
    forward(step)
    pendown()


def mkhand(name, length):
    # 注册turtle形状，建立表针turtle
    reset()
    skip(-length * 0.1)
    begin_poly()
    forward(length * 1.1)
    end_poly()
    handform = get_poly()
    register_shape(name, handform)


def init():
    global sechand, minhand, hurhand, printer
    mode("logo")
    # 重置turtle指向北
    # 建立三个表针turtle并初始化
    mkhand("sechand", 125)
    mkhand("minhand", 130)
    mkhand("hurhand", 90)
    sechand = Turtle()
    sechand.shape("sechand")
    minhand = Turtle()
    minhand.shape("minhand")
    hurhand = Turtle()
    hurhand.shape("hurhand")
    for hand in sechand, minhand, hurhand:
        hand.shapesize(1, 1, 3)
        hand.speed(0)
    # 建立输出文字turtle
    printer = Turtle()
    printer.hideturtle()
    printer.penup()


def setupclock(radius):
    # 建立表的外框
    reset()
    pensize(7)
    for i in range(60):
        skip(radius)
        if i % 5 == 0:
            forward(20)
            skip(-radius - 20)
        else:
            dot(5)
            skip(-radius)
        right(6)


def week(t):
    week = ["星期一", "星期二", "星期三", "星期四", "星期五", "星期六", "星期日"]
    return week[t.weekday()]


def date(t):
    y = t.year
    m = t.month
    d = t.day
    return "%s %d %d" % (y, m, d)


def tick():
    # 绘制表针的动态显示
    t = datetime.today()
    second = t.second + t.microsecond * 0.000001
    minute = t.minute + second / 60.0
    hour = t.hour + second / 60.0
    sechand.setheading(6 * second)
    minhand.setheading(6 * minute)
    hurhand.setheading(30 * hour)
    tracer(False)
    printer.forward(65)
    printer.write(week(t), align="center", font=("Courier", 14, "bold"))
    printer.back(130)
    printer.write(date(t), align="center", font=("Courier", 14, "bold"))
    printer.home()
    tracer(True)
    ontimer(tick, 100)  # 100ms后继续调用tick


def main():
    tracer(False)
    init()
    setupclock(160)
    tracer(True)
    tick()
    mainloop()


main()

class Interface:

    def f1(self):

        '''
            to do something
        :return:
        '''
        raise NotImplementedError

class Something(Interface):

    def f1(self):
        print("to do somethings")

    def f2(self):
        print("to do other")


s = Something()
s.f1()
s.f2()


# 抽象类
class Foo(metaclass=abc.ABCMeta):

    def f1(self):
        print('f1')

    # 抽象方法
    @abc.abstractmethod
    def f2(self):
        '''
        打印f2
        '''


class Bar(Foo):

    def f2(self):
        print('f2')

    def f3(self):
        print('f3')


b = Bar()
b.f1()
b.f2()
b.f3()


class MyType(type):

    def __call__(cls, *args, **kwargs):
        obj = cls.__new__(cls, *args, **kwargs)
        print('在这里面..')
        print('==========================')
        print('来咬我呀')
        obj.__init__(*args, **kwargs)
        return obj

class Foobar(metaclass=MyType):
    def __init__(self):
        self.name = 'alex'

    def f1(self):
        print(self.name)


f = Foobar()

print(f.name)


class Mapper:
    # 在字典里定义依赖注入关系
    __mapper_relation = {}

    # 类直接调用注册关系
    @staticmethod
    def register(cls, value):
        Mapper.__mapper_relation[cls] = value

    @staticmethod
    def exist(cls):
        if cls in Mapper.__mapper_relation:
            return True
        return False

    @staticmethod
    def get_value(cls):
        return Mapper.__mapper_relation[cls]


class MyInjectType(type):
    def __call__(cls, *args, **kwargs):
        obj = cls.__new__(cls, *args, **kwargs)
        arg_list = list(args)
        if Mapper.exist(cls):
            value = Mapper.get_value(cls)
            arg_list.append(value)
        obj.__init__(*arg_list, **kwargs)
        return obj


class Head:

    def __init__(self):
        self.name = 'alex'


class FooTest(metaclass=MyInjectType):

    def __init__(self, h):
        self.h = h

    def f1(self):
        print(self.h)


class Bar(metaclass=MyInjectType):

    def __init__(self, f):
        self.f = f

    def f2(self):
        print(self.f)


Mapper.register(FooTest, Head())
Mapper.register(Bar, FooTest())

b = Bar()
print(b.f)

