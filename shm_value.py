from multiprocessing import Process
from multiprocessing import Value
import os
import time

shm_v = Value('i', 10000)


def task_process():
    for _ in range(100):
        v = shm_v.value  # 获取共享内存的值对象
        v += 1
        shm_v.value = v
        time.sleep(1)


p = Process(target=task_process)
p.start()

for _ in range(200):
    v = shm_v.value
    print("主进程中获取到的值:", v)
    time.sleep(1)


p.join()
print("程序正常退出")



