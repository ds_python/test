# 使用Select IO多路复用实现 (TCP协议，可以实现安全稳定的链接)
# IO 多路复用的弊端
# 当一个IO占用比较长时间，会发生阻塞。是线性排队的
#

import socket
import select

tcp_server = socket.socket()
tcp_server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
tcp_server.bind(("0.0.0.0", 8888))
tcp_server.listen()

rList = [tcp_server]  # 存储IO对象

while True:
    print("等待客户端连接...")
    rs, ws, es = select.select(rList, [], [])
    while r_socket in rs:
        if r_socket is tcp_server:
            user_sock, addr = tcp_server.accept
            print("接收客户端连接:", addr, "总连接数:", len(rList)-1)
            rList.append(user_sock)
        else:
            data = r_socket.recv(1024)
            if data:
                send_data = b'====' + data + b'===='
                r_socket.send(send_data)
            else:
                r_socket.close()
                rList.remove(r_socket)
                print("当前连接数是:", len(rList) - 1)

tcp_server.close()

print("服务器正常关闭")


