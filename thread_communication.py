# 使用全局变量进行进程间通信
from threading import Thread
import time

myvar = 10000

def thread_task():
    global myvar
    for _ in range(20):
        myvar += 1
        print("子线程+1后 myvar =", myvar)
        time.sleep(1)
    print("子线程任务结束")


t = Thread(target=thread_task)
t.start()

def main_task():
    global myvar
    for _ in range(10):
        myvar -= 2
        print("主线程-1后 myvar =", myvar)
        time.sleep(1)

    print("主线程任务结束")


main_task()
t.join()
print("变量的值：", myvar)

