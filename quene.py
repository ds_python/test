import multiprocessing as mp
from multiprocessing import Queue
from multiprocessing import Process
import time
import os

# 创建消息队列
quene = Queue(5)  # 存放5个,队列可以放入多个数据


def task_process(q):  # q绑定传入的消息队列
    while True:
        s = q.get()  # 从消息队列中获取消息
        print("子进程收到消息：", s)
        if not s:
            break
        time.sleep(5)  # 每5秒取出1次消息队列中的数据，取得非空数据
        print("子进程退出")


p = Process(target=task_process, args=(quene,))
p.start()

# 主进程发送消息
while True:
    s = input("请输入要发送给子进程的消息:")
    quene.put(s)
    if not s:  # 如果用户没有输入数据
        break


p.join()
print("主进程退出")


