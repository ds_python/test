from socket import *
import os

# 存放用户
user = {}

def do_login(s, name, addr):
    """
        处理用户进入聊天室
    :return:
    """
    if (name in user) or name == "管理员消息":  # 如果用户已经存在，告诉客户端
        s.sendto("该用户已经存在".encode(), addr)
        return
    else:
        s.sendto(b'OK', addr)

    # 通知其他人
    msg = "欢迎 %s 进入聊天室" % name
    for i in user:
        s.sendto(msg.encode(), user[i])
    # 将用户加入到user
    user[name] = addr



def do_chat(s, name, text):
    # 发送给除了自己的所有人
    msg = "%s 说：%s" %(name, text)
    t = '管理员消息'
    if t in name:
        msg_list = name.split(t)
        msg = msg_list[1]

    for i in user:
        if i != name:
            s.sendto(msg.encode(),user[i])


def do_quit(s, name):
    msg = "%s 退出聊天室" % name
    for i in user:
        if i == name:
            # 本人反馈退出消息
            s.sendto(b'EXIT', user[i])
        else:
            # 群发某人退出聊天室
            s.sendto(msg.encode(), user[i])

    del user[name]


def do_request(s):
    """
        处理请求
        存储结构 { 'zhangsan':('127.0.0.1',8888)}
    :return:
    """
    print("------客户端请求日志------")
    while True:
        msg, addr = s.recvfrom(1024)
        print(msg.decode())
        msg_list = msg.decode().split(' ')
        if msg_list[0] == 'L':
            do_login(s, msg_list[1],addr)
        elif msg_list[0] == 'C':
            text = ''.join(msg_list[2:])  # 合并起来，有可能分割了消息
            do_chat(s, msg_list[1], text)
        elif msg_list[0] == 'Q':
            do_quit(s, msg_list[1])


def main():
    addr = ('0.0.0.0', 8888)
    s = socket(AF_INET, SOCK_DGRAM)
    s.bind(addr)

    # 创建父子进程，用于处理客户端请求和发送管理员消息
    pid = os.fork()
    if pid < 0:
        print("Process error")
    elif pid == 0:
        # 子进程处理管理员发送消息
        while True:
            msg = input("管理员消息:")
            msg = "C 管理员消息" + msg
            s.sendto(msg.encode(), addr)  # 父进程发送消息执行do_request
        pass
    else:
        # 调用函数处理处理客户端请求
        do_request(s)
        pass



# 收到消息，然后转发
# 管理消息，随时会发送。单独创建进程，随时发送

if __name__ == "__main__":
    main()