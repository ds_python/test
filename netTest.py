import OpenSSL
import socket
import socketserver

# OSI 七层网络模型
# 应用层 Http,ftp,smtp,pop3,https
# 表示层 jpg,ascii wav mp3
# 回话层 建立会话
# 网络层 路由器，选择网络的
# 数据链路层  mac 地址,交换机
# 传输层 提供传输服务，流量控制
# 物理层  二进制传输

# OSI 分部清晰
# OSI 降低网络耦合
# 套接字用来网络通信的对象

# socket 实现网络编程，数据传输接口
# udp_sock = socket.socket(scoket.AF_INET,socket.SOCK_DGRAM,0)
# udp.send(b"hello",("192.168.55.100",5000))
# 创建套接字 bind ---> 发送和接收数据，关闭套接字
# updp socket(地址，套接字类型，协议)
# udp_sock.bind(地址和端口号)
# udp_sock.sendto(要发送数据的字节串，地址)
# 参数



# TCP/IP 协议
# 应用层
# 传输层 TCP/IP
# 网络层 IP
# 物理链路层
# 网段
# 网关
# 端口号  用来区分一台主机上的进程，端口号 0 ~ 65535
# http 80 ftp 23 ssh 22
# 1024 ~ 49151 为注册端口
# 其他为动态端口，系统自动分配
# ipv4 32 位
# ipv6 128 位
# 127.0.0.1
# 192.168.56.129
# udp 协议，视屏直播，数据报文协议
# ifconfig
# ipconfig



class CNNSockedServerManager(socketserver.BaseRequestHandler):

    def __init__(self):
        pass

    def udp_conenct(self):
        # 1.创建套接字
        udp_server = socket.socket(socket.AF_INET,socket.SOCK_DGRAM,0)
        # 2.绑定 56.129
        # server_addr = ('192.168.43.30',8888)
        server_addr = ('0.0.0.0',8888)

        udp_server.bind(server_addr)
        print("正在等待接收数据")

        while True:
            # 3.发送和接收,左右各加4个=，再返回给客户端
            recv_bytes,client_addr = udp_server.recvfrom(1024)
            # 4.关闭套接字
            print(recv_bytes,client_addr)
            send_bytes = b'====' + recv_bytes + b'===='
            udp_server.sendto(send_bytes,client_addr)
        udp_server.close()  # 关闭套接字

    def handle(self):
        conn = self.request
        conn.sendall(bytes("你好，欢迎登陆！", encoding="utf-8"))

        while True:
            # 输出等待客户端发送内容
            print("正在等待Client输入内容......")
            # 接收客户端发送过来的内容
            ret_bytes = conn.recv(1024)
            # 转换成字符串类型
            ret_str = str(ret_bytes, encoding="utf-8")
            # 输出用户发送过来的内容
            print(ret_str)
            # 如果用户输入的是q
            if ret_str == "q":
                # 则退出循环，等待下个用户输入
                break
            # 给客户端发送内容
            inp = input("Service请输入要发送的内容>>> ")
            conn.sendall(bytes(inp, encoding="utf-8"))

if __name__ == "__main__":
    #server = socketserver.ThreadingTCPServer(('127.0.0.1',8080),CNNSockedServerManager())
    #server.serve_forever()

    server = CNNSockedServerManager()
    server.udp_conenct()

