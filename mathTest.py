#import math

# 可以改模块的名字
# import math as mc
# 如何使用from 包 import 函数 语句

from sympy import *
from math import *




# t = Symbol('t')
# x = Symbol('x')
# m = integrate(sin(t)/(pi-t),(t,0,x))
# n = integrate(m,(x,0,pi))
#
# print(n)


def get_area():
     r = int(input("请输入一个圆的半径:"))
     return pow(r,2) * (pi)

def get_r():
    s = int(input("请输入一个圆的面积:"))
    return sqrt(s / pi)


s = get_area()
print("圆的面积 : ",s)


#r = get_r()
#print("圆的半径",r)

