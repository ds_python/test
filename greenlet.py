# 函数：

from greenlet import greenlet

def task1():
    print("1")
    print("2")
    print("3")


def task2():
    print("A")
    print("B")
    print("C")


gr1 = greenlet(task1)
gr2 = greenlet(task2)  # 创建协程对象

gr1.switch()  # 切换到gr1协程，让他先运行
print("程序退出")

