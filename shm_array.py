from multiprocessing import Process
from multiprocessing import Array
import time

shm_a = Array('c', 100)  # 初始值都为0


def task_process():
    for i in range(100):
        with shm_a:
            shm_a[i] = b'A'  # 将共享数组的第一个字节改b'A'
        time.sleep(0.1)


p = Process(target=task_process)
p.start()


def task_process2():
    for _ in range(110):
        with shm_a:
            v = shm_a.value  # shm_a.raw
            print("数据的长度是:", len(v), 'v=', v)
        time.sleep(0.1)


task_process2()
print("wait")

p.join()

print("程序正常退出")
