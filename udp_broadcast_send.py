import socket

class CNNUdpBroadcastSend:
    def __init__(self):
        pass

    def connect(self):
        # 1.创建udp的套接字
        udp_sender = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
        # 2.未发送和广播设置套接字选项
        udp_sender.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        # 3.定时发送广播信息
        broadcast_addr = ("192.168.43.255",9999)
        import time
        for x in range(100):
            time.sleep(2)
            udp_sender.sendto("北京天冷了多穿点!".encode("utf-8"),broadcast_addr)
            print("已经成功发送")

        udp_sender.close()  #关掉


if __name__ == "__main__":
    sender = CNNUdpBroadcastSend()
    sender.connect()