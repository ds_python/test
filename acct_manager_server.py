# 服务器端
from show_dbs import CNMongoDBBase
from socket import *

db_host = "127.0.0.1"
db_port = 27017
db_name = "bank"
db_conn = None  # 数据库链接对象


class CNNAccountManager(CNMongoDBBase):
    """
        CRUD 实现基本的增删改查询
        请求 （自定义的protocol）
            query::all 表示查询所有账号
            query::62223458888 表示查询某个账户的数据
        响应
            查询成功，共1笔记录
            账号：62223458888，户名：Tom, 余额：1
    """
    def __init__(self):
        pass

    def server_main(self):
        """
        建立启动网络服务器
        :return:
        """
        server_tmp = socket()
        server_tmp.bind(("127.0.0.1", 9999))
        server_tmp.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
        server_tmp.listen(5)  # 监听端口，允许5个等等
        print("服务器已经启动")

        socket_fd, addr = server.accept()  #接收客户端链接
        while True:
            data = socket_fd.recv(2048)  # 一次最多2k数据
            if not data:
                print("客户端已关闭")
                break
            print(data.decode())  # 打印请求信息



if __name__ == "__main__":
    # CNMongoDBBase.cn_query()
    server = CNNAccountManager()
    server.server_main()

    #cn_mongo_base = CNMongoDBBase('')


