# 使用全局变量进行进程间通信
from threading import Thread
from threading import Lock
import time

myvar = 10000
alock = Lock()

def thread_task():
    global myvar
    for _ in range(1000000):
        #with alock:
        alock.acquire()  # 在此处死锁
        myvar += 1
        print("子线程+1后 myvar =", myvar)
        #time.sleep(1)
        alock.release()
    print("子线程任务结束")


t = Thread(target=thread_task)
t.start()

def main_task():
    global myvar
    for _ in range(1000000):
        alock.acquire()
        myvar -= 1
        print("主线程-1后 myvar =", myvar)
        # time.sleep(1)
        alock.release()

    print("主线程任务结束")


main_task()
t.join()
print("变量的值：", myvar)



