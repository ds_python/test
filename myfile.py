# 0d0a 换行符
# len(L),可以读取有几个对象
import os
from math import floor
from math import ceil
import requests

# L = ['1','2','3']
# t = len(L) # 判断列表个数
# print("t:",t)
#
# for s in L:
#     print("s :",s)

#文本文件 odoa都会转换成\b,默认转换
class BaseHandler(object):
    '''自定义回调函数'''
    def crawl(self, url, **kwargs):
        if kwargs.get('callback'):
            callback = kwargs['callback']
            print("callback:",callback)
            #if hasattr(self, callback):
            #func = getattr(self, callback)

        resp = requests.get(url)
        return callback(resp)
        #return func(resp)

class Anjuke(BaseHandler):
    def on_start(self):
        self.crawl("http://www.baidu.com", callback=self.index_url)

    def index_url(self, response):
        print(response.text)


a = Anjuke()
print(a.on_start())


class Singleton(object):
    ''''' A python style singleton '''
    def __new__(cls, *args, **kw):
        if not hasattr(cls, '_instance'):
            org = super(Singleton, cls)
            cls._instance = org.__new__(cls, *args, **kw)
        return cls._instance


class Person(Singleton):
    def __init__(self, s):
        self.s = s

    def __str__(self):
        return self.s

s2 = Person('spa')
print(s2.s)




def write_data_to_file(write="a"):
    while True:
        name = input("请输入姓名:")
        if name == '':
            print("\n输入已经结束:\n")
            read_file()
            return False

        age = input("请输入年龄:")
        score = input("请输入分数:")

        try:
            age = int(age)
            score = int(score)

            #解决输入的数字不在指定范围的问题,断言输入正确条件和其他语言不一样
            assert age >= 0 and age <= 100 ,'年龄输入不合法'
            assert score >= 0 and score <= 100 , '分数输入不合法'

        except ValueError as e:
            print(e.args[0])
            print("请输入正确的数字格式")
            break
        except AssertionError as e:
            print(e.args[0])
            break

        real_write(name,str(age),str(score),write=write)

        break


def real_write(name,age,score,write="w"):
    try:
        with open("mydata.txt",write) as f:
            result = f.write(name + " " + age + " " + score + "\n",)
            print(end='\r\n')
        f.close()

        # 再次提示写入操作
        write_data_to_file("a")

    except OSError as e:
        print("e:",e.args)
    except UnicodeEncodeError as e:
        print("e",e.args)

def clear_file():
    try:
        with open("mydata.txt", "r+") as f:
            f.truncate()
            print("清空文件")
        f.close()
    except OSError as e:
        print(e.args)

def read_file(path=""):
    #L = []
    try:
        with open("mydata.txt","r") as f:
            s = f.read()
            ##s = s.strip() #去掉空格
            #s = s.split()
            print(s)
    except Exception as e:
        print("e",e.args)

def myR(fn):

    def myround(x):
        '''
            四舍五入函数
        :param x:
        :return:
        '''
        arv = (ceil(x) + floor(x)) / 2
        print('myround 函数被调用了')

        if x < arv:
            fn(floor(x))
            return floor(x)
        else:
            fn(ceil(x))
            return ceil(x)

    return myround

# 说明装饰器函数里面的参数和调用函数的参数是公用的
@myR
def read_pic_file(x=1):
    '''
        读取二进制文件，必须rb
    :param type:
    :return:
    '''
    print("11111")
    try:
        with open("6.jpg","rb") as f:
            s = f.read()
            #b = s.decode()
            #x = len(s)/ 1024x
            #kb = myR(x)
            print(x)
            #x = len(s)/ 1024

            #print('读取到的字节个数: %s kb'% kb,end=' ')

    except OSError as e:
        print("e",e.args)


def make_power(y):
    def fn(x):
        print(y)
        return x ** y
    return fn


#a = make_power(3)(2)
#print("makepower:",a)

#clear_file()

#测试
#write_data_to_file()

#read_file()

# 必须在调用函数加参数,如果装饰器函数不调用原来的函数
# 这个只能对传入的形参有作用，对函数内部的参数没有任何影响，对原来函数不提供功能性作用

#read_pic_file(x=5.6)

#print(myR(1))





