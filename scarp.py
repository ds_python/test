# 网络请求库
import requests
# 可以快速解析网页
from bs4 import BeautifulSoup

import time

#import pymongo
from pymongo import MongoClient

from lxml import etree

headers = {
    'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'
    }

res = requests.get('http://bj.xiaozhu.com/')

res = requests.get('http://bj.xiaozhu.com/',headers = headers)

# 除了GET 方法还有
#print(res)

#client = pymongo.MongoClient('localhost',27017)
client = MongoClient('localhost',27017)

mydb = client.local
test = mydb.test

try:
    #print(res.text)
    # 解析网页
    soup = BeautifulSoup(res.text, 'html.parser')
    print(soup.prettify())

    soup.find_all('div',"item") # 查找div标签，class="item"
    #soup.find_all('div',class='item')
    soup.find_all('div',attrs={"class":"item"}) # attr 参数定义了一个字典参数来搜索包含特殊属性的tag
    #find 返回的是 'bs4.element.Tag'
    #find_all 返回的是
    #soup.selector(div.item > a > h1)

    #price = soup.select('#page_list > ul > li:nth-of-type(1) > div.result_btm_con.lodgeunitname > span.result_price   >   i')
    # 定位元素位置并通过selector方法提取

    #page_list > ul > li > div.result_btm_con.lodgeunitname > span.result_price > i
    prices = soup.select('#page_list > ul > li > div.result_btm_con.lodgeunitname > span.result_price > i')
    # 可以得到整个页面的所有价格，这样提取的信息为列表，可以通过循环打印出来。也可以存储起来
    for price in prices:
        print(price.get_text)

except ConnectionError: # 出现错误会执行下面的操作
    print('拒绝链接')

def judgement_sex(class_name):
    if class_name == ['member_icon1']:
        return '女'
    else:
        return '男'

def get_info(url):

    wb_data = requests.get(url,headers = headers)
    soup = BeautifulSoup(wb_data.text,'lxml')
    titles = soup.select('dev.pho_info > h4')
    addresses = soup.select('span.pr5')
    prices = soup.select('#pricePart > div.day_l > span')
    images = soup.select('#floatRightBox > div.js_box.clearfix > div.member_pic > a > img')
    names = soup.select('#floatRightBox > div.js_box.clearfix > div.w_240 > h6 > a')
    sexs = soup.select('#floatRightBox > div.js_box.clearfix > div.member_pic > div')

    for title,addresses,price,img,name,sex in zip(titles,addresses,prices,images,names,sexs):
        data = {
            'title':title.get_text().strip(),
            'address':addresses.get_text().strip(),
            'price':price.get_text().strip(),
            'img':img.get_text().strip(),
            'name':name.get_text().strip,
            'sex':judgement_sex(sex.get("class"))
        }
        print(data)

    # 把数据插入到mongo
    test.insert({'title':data['title'],'address':data['address'],'price':'Cleveland','img':data['img'],'name':data['name'],'sex':data['sex']})


def get_links(url):
    wb_data = requests.get(url,headers = headers)
    soup = BeautifulSoup(wb_data.text,'lxml')
    links = soup.select('#page_list > ul > li >a') #links为URL列表
    for link in links:
        href = link.get("href")
        get_info(href)


# 程序入口

if __name__ == '__main__':

    urls = ['http://bj.xiaozhu.com/search/search-duanzufang-p{}-0'.format(number) for number in range(1,100)]

    # 获取100页的数据
    for single_url in urls:
        get_links(single_url)
        time.sleep(5)



