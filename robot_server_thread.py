# 使用Select IO多路复用实现 (TCP协议，可以实现安全稳定的链接)
# IO 多路复用的弊端
# 当一个IO占用比较长时间，会发生阻塞。是线性排队的
#

import socket
import select
from multiprocessing import Process
from threading import Thread
from threading import Lock

import time

tcp_server = socket.socket()
tcp_server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
tcp_server.bind(("0.0.0.0", 8888))
tcp_server.listen()

tasks = []
count = 0
alock = Lock()


def task(user_socket):  # 绑定用户套接字
    global count
    while True:
        data = user_socket.recv(1024)
        if data:
            send_data = b'====' + data + b'===='
            time.sleep(5)
            user_socket.send(send_data)
        else:
            user_socket.close()
            break  # 退出进程或线程
    with alock:
        count -= 1  # 把计数-1


while True:
    print("等待客户端连接...")
    user_sock, addr = tcp_server.accept()
    with alock:
        count += 1
    print("接收客户端连接:", addr, "总连接数:", count)
    t = Thread(target=task, args=(user_sock,))
    t.start()
    tasks.append(t)

tcp_server.close()

print("服务器正常关闭")


