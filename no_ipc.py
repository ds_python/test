# no_ipc.py
import os
from time import sleep
import multiprocessing as mp

myvar = 10000


def process_task():
    """
        子进程处理函数
    :return:
    """
    global myvar
    for _ in range(30):
        myvar -= 1
        sleep(1)
        print("PID为:", os.getpid(), '的子进程的myvar=', myvar)


p = mp.Process(target=process_task)
p.start()  # 启动子进程

for _ in range(50):
    myvar += 2
    sleep(1)
    print("PID为:", os.getppid(), '的父进程的myvar=', myvar)

p.join()  # 回收子进程资源
print("myvar=", myvar)