import socket
import select

tcp_client = socket.socket()
tcp_client.connect(("0.0.0.0", 8888))

while True:
    s = input("请输入要发送给机器人的话:")
    if not s:
        break
    b = s.encode()
    tcp_client.send(b)

    recv_bytes = tcp_client.recv(1024)
    print("机器人说:", recv_bytes.decode())

