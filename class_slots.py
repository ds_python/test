
class Human:
    # human 只能有name,age属性,__slots__列表
    __slots__ = ['name', 'age']

    def __init__(self, n, a):
        self.name, self.age = n, a

    def show_info(self):
        print(self.name, "今年", self.age, "岁")


s1 = Human("小张", 10)
s1.show_info()

# s1.Age = 19 # 区分打下写，会创建多个Age属性
# s1.show_info()

# print(s1.__dict__) # 不存在的


class A:
    v = 0
    @classmethod
    def get_v(cls):
        return cls.v
    @classmethod
    def set_v(cls, value):
        cls.v = value

# 不建议直接操作属性，引起数据混乱

print(A.v)

A.set_v(100)

var = A.get_v()
print(var)

# 用方法来修改属性，而不是直接赋值
# 逻辑都封装到方法里面，处理问题比较快
#










