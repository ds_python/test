# 利用gevent实现协程
import gevent
import greenlet


def task1(a, b):
    print("任务1开始运行...", a, b)
    # 此处的时间需要处理5秒
    gevent.sleep(5)  # 让出cpu,且安魂到另一个协程
    print("此任务1结束运行")
    return 8888


def task2():
    print("任务2开始运行...")
    print("任务2结束运行")
    return 9999


gr1 = gevent.spawn(task1, 100, 200)
gr2 = gevent.spawn(task2)

L = [gr1, gr2]
result = gevent.joinall(L)  # 让L中的两个协程对象运行


for grnlet in result:
    print(grnlet.get())  # 8888或 9999

# print(result[1].get)

print("程序结束 result =", result)

# 修改 gevent内的阻塞函数，让起能在阻塞之前主动让出协程控制权
# 控制权
# from gevent import monkey
# monkey.patch_all()  # 为IO函数打补丁来让IO函数能够主动让出协程控制权
#







