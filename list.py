#import django
#import Fibo
import sys
import math
import random

from PIL import Image
import pytesseract


# 多线程模板
import threading, zipfile

# 这个模块支持互联网访问
from urllib.request import urlopen


stack = [3,4,5]
stack.append(6)
stack.append(7)

stack.pop()
print(stack)
print()

# 元组
t = 12345,87,'hello'


#Python 还包含了一个数据类型 —— set (集合)。集合是一个无序不重复元素的集
basket = {'apple', 'orange', 'apple', 'pear', 'orange', 'banana'}

#字典用法
tel = {'jack': 4098, 'sape': 4139}
print(tel['jack'])

#字典遍历
knights = {'gallahad': 'the pure', 'robin': 'the brave'}

# 打印遍历前面必须加上%，且不能有','号隔开
# 只支持一个变量格式化输出？

for k, v in knights.items():
    #print('%s' % k)
    print('key:{} value:{}'.format(k,v),end=',')

print()
print('------------')

# 列表遍历输出
for i, v in enumerate(['tic', 'tac', 'toe']):
    print(i, v)

#Fibo.fab(100)

#print(Fibo.fab(100))


# 内置函数 dir() 用于按模块名搜索模块定义，它返回一个字符串类型的存储列表:
#print(dir(sys),end=' ')

print('We are the {} who say "{}!"'.format('knights', 'Ni'))

try:
    f = open('myfile.txt')
    s = f.readline()
    i = int(s.strip())
except OSError as err:
    print("OS error: {0}".format(err))
except ValueError:
    print("Could not convert data to an integer.")
except:
    print("Unexpected error:", sys.exc_info()[0])
    raise # 抛出异常


class MyClass:
    """A simple example class"""
    i = 12345
    def f(self):
        return 'hello world' + str(i)

print(MyClass().f())


class Dog:
    kind = 'canine'         # class variable shared by all instances
    def __init__(self, name):
        self.name = name


d = Dog('Fido')
print(d.name)


# 继承用法
class Mapping:
    def __init__(self, iterable):
        self.items_list = []
        self.__update(iterable)
    def update(self, iterable):
        for item in iterable:
            self.items_list.append(item)
    __update = update   # private copy of original update() method

# 多继承用法
# class DerivedClassName(Base1, Base2, Base3):
class MappingSubclass(Mapping):
    def update(self, keys, values):
        # provides new signature for update()
        # but does not break __init__()
        for item in zip(keys, values):
            self.items_list.append(item)



#了解了迭代器协议的后台机制，就可以很容易的给自己的类添加迭代器行为。定义一个 __iter__() 方法，使 其返回一个带有 __next__() 方法的对象。如果这个类已经定义了 __next__() ，那么 __iter__() 只需要返回
# 可以返回数据
# yield

'tea for too'.replace('too', 'two')


print(random.random())

# for line in urlopen('http://tycho.usno.navy.mil/cgi‐bin/timer.pl'):
#         line = line.decode('utf‐8')  # Decoding the binary data to text.
#         if 'EST' in line or 'EDT' in line:  # look for Eastern Time
#             print(line)

# class AsyncZip(threading.Thread):
#     def __init__(self, infile, outfile):
#         threading.Thread.__init__(self)
#         self.infile = infile
#         self.outfile = outfile
#     def run(self):
#         f = zipfile.ZipFile(self.outfile, 'w', zipfile.ZIP_DEFLATED)
#         f.write(self.infile)
#         f.close()
#         print('Finished background zip of:', self.infile)
#
#
# background = AsyncZip('mydata.txt', 'myarchive.zip')
# background.start()
# print('The main program continues to run in foreground.')
# background.join()    # Wait for the background task to finish
# print('Main program waited until background was done.')


#上面都是导包，只需要下面这一行就能实现图片文字识别
#text=pytesseract.image_to_string(Image.open('denggao.jpeg'),lang='chi_sim')

#图像识别 /Users/dushuzhong/Desktop
image = Image.open('/Users/dushuzhong/Desktop/hello.jpeg')
print(image)

#pytesseract.image_to_string(image,lang='chi_sim')
print(pytesseract.get_tesseract_version())

# text = pytesseract.image_to_string(image,lang='chi_sim')
#
# print(text)
