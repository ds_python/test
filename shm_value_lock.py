# 对共享资源进行互斥操作

from multiprocessing import Process
from multiprocessing import Value
from multiprocessing import Lock
import os
import time

# 创建一个共享内存对象
shm_v = Value('i', 10000)
# 用多进程锁进行互斥操作
# 创建一个锁用来保护对共享值对象的操作
shm_lock = Lock()


def task_process():
    for _ in range(1000000):
        with shm_lock:
            v = shm_v.value  # 获取共享内存的值对象
            v += 1
            shm_v.value = v
            # time.sleep(1)


p = Process(target=task_process)
p.start()

for _ in range(1000000):
    with shm_lock:
        v = shm_v.value
        v -= 1
        shm_v.value = v
        #print("主进程中获取到的值:", v)
        #time.sleep(1)


print("共享内存中的值", shm_v.value)
# time.sleep(1)

# shm_v.close()
p.join()
print("程序正常退出,mysql事务的概念，就是为了避免互斥")





