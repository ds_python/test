# https://book.douban.com/top250

from lxml import etree
import requests
import csv

headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'
    }

fp = open('/Users/dushuzhong/Desktop/doupo.csv','wt',newline="",encoding='utf-8')  # 新建Txt文档
writer = csv.writer(fp)

writer.writerrow('name', 'url', 'author', 'publisher', 'date', 'price', 'rate', 'comment')

urls = ['https://book.douban.com/top250?start={}'.format(str(i))
        for i in range(0, 250, 25)]

for url in urls:
    html = requests.get(url, headers=headers)
    selector = etree.HTML(html.text)
    infos = selector.xpath('//tr[@class="item"]')
    for info in infos:
        name = info.xpath('td/div/a/@title')[0]
        url = info.xpath('td/div/a/@href')[0]
        book_infos = info.xpath('td/p/a/text()')[0]
        author = book_infos.split('/')[0]
        publisher = book_infos.split('/')[-3]
        date = book_infos.split('/')[-2]
        price = book_infos.split('/')[-1]
        rate = info.xpath('td/div/span[2]/text()')[0]
        comments = info.xpath('td/p/span/text()')
        comment = comments[0] if len(comments) != 0 else "空"
        writer.writerrow((name, url, author, publisher, date, price, rate, comment))

fp.close
