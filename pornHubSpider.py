import requests
import logging

from scrapy.spiders import CrawlSpider
from scrapy.selector import Selector
from items import PornVideoItem
from scrapy.http import Request
import re
import json
import random
import csv

"""
    归纳PornHub资源链接
"""
PH_TYPES = [
    '',
    'recommended',
    'video?o=ht',  # hot
    'video?o=mv',  # Most Viewed
    'video?o=tr'  # Top Rate
]

class Spider(CrawlSpider):

    name = 'pornHubSpider'
    host = 'https://www.pornhub.com'
    start_urls = list(set(PH_TYPES))

    # 将requests的日志级别设成WARNING
    # loging配置
    logging.getLogger("requests").setLevel(logging.WARNING
                                          )
    logging.basicConfig(
        level=logging.DEBUG,
        format=
        '%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',
        datefmt='%a, %d %b %Y %H:%M:%S',
        filename='cataline.log',
        filemode='w')

    # test = True
    def start_requests(self):
        for ph_type in self.start_urls:
            yield Request(url='https://www.pornhub.com/%s' % ph_type,
                          callback=self.parse_ph_key)
    # 解析函数
    def parse_ph_key(self, response):
        # 使用Selector解析
        selector = Selector(response)

        logging.debug('request url:------>' + response.url)
        # logging.info(selector)
        divs = selector.xpath('//div[@class="phimage"]')

        for div in divs:
            # logging.debug('divs :------>' + div.extract())

            viewkey = re.findall('viewkey=(.*?)"', div.extract())

            logging.debug(viewkey)
            yield Request(url='https://www.pornhub.com/embed/%s' % viewkey[0],
                          callback=self.parse_ph_info)

        url_next = selector.xpath(
            '//a[@class="orangeButton" and text()="Next "]/@href').extract()
        logging.debug(url_next)

        if url_next:
            # if self.test:
            logging.debug(' next page:---------->' + self.host + url_next[0])
            yield Request(url=self.host + url_next[0],
                          callback=self.parse_ph_key)
            # self.test = False

    # 解析数据详情
    def parse_ph_info(self, response):

        phItem = PornVideoItem()
        selector = Selector(response)
        # logging.info(selector)
        _ph_info = re.findall('var flashvars =(.*?),\n', selector.extract())
        logging.debug('PH信息的JSON:')
        logging.debug(_ph_info)
        _ph_info_json = json.loads(_ph_info[0])
        duration = _ph_info_json.get('video_duration')
        phItem['video_duration'] = duration
        title = _ph_info_json.get('video_title')
        phItem['video_title'] = title
        image_url = _ph_info_json.get('image_url')
        phItem['image_url'] = image_url
        link_url = _ph_info_json.get('link_url')
        phItem['link_url'] = link_url
        quality_480p = _ph_info_json.get('quality_480p')
        phItem['quality_480p'] = quality_480p
        logging.info('duration:' + duration + ' title:' + title + ' image_url:'
                     + image_url + ' link_url:' + link_url)


        #csvfile = file('pornhub.csv', 'wb')
        #writer = csv.writer(csvfile)

        #先写入columns_name
        #writer.writerow(['duration', 'title', 'image_url','link_url','quality_480p'])
        #writer.writerow([duration, title, image_url,link_url,quality_480p])

        yield phItem
