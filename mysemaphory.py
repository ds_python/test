# 多进程实现互斥

from multiprocessing import Process
from multiprocessing import Semaphore
import os
import time

# 创建信号量
sem = Semaphore(0)


def task_process():
    print("子进程PID=", os.getpid(), "的进程正在等待抢手机")

    sem.acquire()
    print("子进程 PID=", os.getpid(), "的进程已获取手机下单权限，正在操作中")
    time.sleep(3)
    print("子进程PID=", os.getpid(), "下单成功！")


process = []
for i in range(10):
    p = Process(target=task_process)
    process.append(p)
    p.start()

count = int(input("请输入手机个数"))
for i in range(count):
    sem.release()

for p in process:
    p.join()



