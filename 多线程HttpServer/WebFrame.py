# 模拟web框架部分 类似于

from socket import *
from select import select

from views import *

frame_ip = '127.0.0.1'
frame_port = 8080
frame_address = (frame_ip, frame_port)

# 静态网页目录
STATIC_DIR = './static'
# 可以提前获取的数据列表
urls = [
    ('/time', show_time),
    ('/hello',say_hello)
]

class Application(object):
    def __init__(self):
        self.socketfd = socket()
        self.socketfd.setsockopt(SOL_SOCKET,SO_REUSEADDR,1)
        self.socketfd.bind(frame_address)
        self.rlist = [self.socketfd]
        self.wlist = []
        self.xlist = []

    def start(self):
        self.socketfd.listen(5)
        print("Listen the port %d" %frame_port)

        # IO 多路复用 select
        while True:
            import select
            rs, ws, xs = select(self.rlist, self.wlist, self.xlist)
            for r in rs:
                if r is self.socketfd:
                    conn, addr = r.accept()
                    self.rlist.append(conn)
                else:
                    request = r.recv(1024).decode()  # 接收客户端发送过来的消息
                    if not request:
                        self.rlist.remove(r)  # 客户端退出
                        continue
                    self._handle(r, request)


    def _get_html(self, connfd, path_info):
        if path_info == '/':
            get_file = STATIC_DIR + '/index.html'
        else:
            get_file = STATIC_DIR + path_info

        try:
            fd = open(get_file)
        except IOError as e:
            response = '404'
        else:
            response = fd.read()
        finally:
            connfd.send(response.encode())  # 发送给HttpServer


    def _get_data(self, connfd, path_info):
        """
        获取数据请求
        :param connfd:
        :param path_info:
        :return:
        """
        response = '404'
        for url , func in urls:
            if path_info == url:
                response = func
                connfd.send(response.encode())
                break
            return
        connfd.send(response.encode())


    def _handle(self, connfd, request):
        print(request)
        request_type = request.split(' ')[0]
        path_info = request.split(' ')[1]

        if request_type == 'POST':
            pass
        elif request_type == 'GET':
            if path_info == '/' or path_info[-5:] == '.html':
                self.get_html(connfd, path_info)

                pass
            else:
                self.get_data(connfd, path_info)

        connfd.send(b'ok')



if __name__ == "__main__":
    app = Application()
    app.start()  #启动框架程序