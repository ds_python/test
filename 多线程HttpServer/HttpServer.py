
from socket import *

# 创建多线程的并发
# IO多路复用
# 假的Nginx,Apache

from threading import Thread

# 导入环境配置文件
from 多线程HttpServer.HttpServerSerttings import *

# 客户端链接
def connect_frame(request_type, path_info):
    s = socket()
    try:
        s.connect(frame_address)
    except Exception as e:
        print(e)
        return
    request = request_type + ' ' + path_info
    s.send(request.encode())

    res = s.recv(4096).decode()  # 4096可能不够
    print(res)
    return res



class HttpServer(object):
    def __init__(self, address):
        self.address = address
        self.socketfd = HttpServer.create_scoket()
        self.bind(address)


    # 创建套接字
    @staticmethod
    def create_scoket():
        socketfd = socket()
        socketfd.setsockopt(SOL_SOCKET,SO_REUSEADDR,1)
        return socketfd


    def bind(self,address):
        """

        :param address:
        :return:
        """
        self.ip = address[0]
        self.port = address[1]
        self.socketfd.bind(address)


    def server_forever(self):
        """
        启动服务器
        :return:
        """
        self.socketfd.listen(10)
        print("Listen the prot %d..." % self.port)

        while True:
            conn, addr = self.socketfd.accept()
            print("connect from ", addr)
            handle_client = Thread(target=self.handle, args=(conn, ))
            handle_client.setDaemon(True)  #主线程退出，分支线程退出
            handle_client.start()


    def handle(self, conn):
        """
        处理具体的客户端请求
        :param conn:
        :return:
        """
        request = conn.recv(1024)
        # print(request)
        if not request:
            conn.close()
        request_lines = request.splitlines()  # bytes格式字符串按行切割
        requestLine = request_lines[0].decode()
        print(requestLine)

        env = requestLine.split(' ')[:2]
        # request_type = env[0]
        # path_info = env[1]

        response = connect_frame(*env) # 解析元组和列表
        # 把数据回发给浏览器
        self.send_response(conn, response)


    def send_response(self, connfd, response):

        response_handles = ''
        response_body = ''

        if response == '404':
            response_handles = "HTML/1.1 404 NOT Found\r\n"
            response_handles +='\r\n'
            response_body = "Sorry not found"

        else:
            response_handles = "HTML/1.1 404 200 Found\r\n"
            response_handles += '\r\n'
            response_body = "ok"

            response = response_handles + response_body
            connfd.send(response.encode())  # 把结果返回给浏览器


if __name__ == "__main__":

    httpd = HttpServer(ADDR)
    httpd.server_forever()  # 启动服务