# http_server_select.py
# 此示例以IO多路复用方式实现HTTP server

from gevent import monkey
monkey.patch_all  # monkey 这个必须在导入socket前调用
# from socket import *
import select
from gevent.socket import socket
import gevent


def get_static_file(url):
    static_dir = './static'
    if url == '/':
        filename = static_dir + '/index.html'
    else:
        filename = static_dir + url
    print("filename =", filename)
    try:
        with open('filename', 'rb') as fr:
            return fr.read()  # 如果文件存在则返回文件内容
    except OSError:
        return b''


def process_client_request(client_socket):
    request = client_socket.recv(4096)  # 接收浏览器传入的数据

    if not request:  # 断开连接
        client_socket.close()

    # 按字节串换行切割，把请求行取出
    request_head = request.splitlines()  # 按'\r\n'
    print("请求的第一行内容是:", request_head[0].decode())

    # 取出地址url
    url = request_head[0].split()[1]

    print("URL:", url)

    data = get_static_file(url)  # 按url读取文件内容

    if not data:  # 读取失败
        print('b=', request)
        s = request.decode()
        print("s=", s)

        response = b'HTTP/1.1 404 Not Found'  # 响应头
        response += b'\r\n'  # 响应头结束
        response += b'This Page Not Found'  # 响应体
        client_socket.send(response)

    else:  # 读取成功
        response = b'HTTP/1.1 200 OK\r\n'
        response += b'\r\n'
        client_socket.send(response)
        client_socket.send(data)

    client_socket.close()  # 关闭客户端按字节


def main_task():
    from socket import *
    server_addr = ('0.0.0.0', 8888)
    tcp_server = socket()
    tcp_server.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
    tcp_server.bind(server_addr)
    tcp_server.listen()

    rlist = [tcp_server]  # 准备让select接收这个IO
    # 加入多进程
    from multiprocessing import Process
    # 导入多线程
    from threading import Thread

    while True:
        print("select...")
        rs, ws, es = select.select(rlist, [], [])
        for r_sock in rs:
            if r_sock is tcp_server:
                user_socket, addr = tcp_server.accept()
                print("addr=", addr)
                # p = Process(target=process_client_request, args=(user_socket))
                # p.start()
                # user_socket.close()
                # 在此处回收已经死亡的线程, is_alive
                # 创建协程greenlet对象
                p = gevent.spawn(process_client_request, user_socket)

                rlist.append(user_socket)
            else:
                process_client_request(r_sock)
                rlist.remove(r_sock)

    tcp_server.close()

# 协程的方式实现
# 把主任务函数也定位协程

m = gevent.spawn(main_task)

print("协程进行等待")
m.join()
print("程序退出")

main_task()

