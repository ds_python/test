import pymysql

db = pymysql.connect("localhost", "root", "12345678", "db5", charset="utf8")
cursor = db.cursor()

def connect_mysql():
    """
        链接数据库
    :return:
    """
    cursor.execute('insert into t1(name, score) values("taoyuanming",88)')
    cursor.execute('insert into t1(name, score) values("taotao",78)')
    cursor.fetchone()  # 一条数据
    cursor.fetchall()
    cursor.fetchmany(4)  # 取n条数据

    db.commit()

    cursor.close()
    db.close()

def delete_data_mysql(n='dashentong'):
    delete = 'delete from t1 where name="%s"'
    cursor.execute(delete, n)
    db.commit()
    cursor.close()
    db.close()

def update_data_mysql(name="李白", score=100):
    upd = 'update t1 set score = 100 where name="李白"'
    cursor.execute(upd)
    db.commit()
    cursor.close()
    db.close()

def insert_data_to_mysql(n=None,s=None):
    ins = 'insert into t1(name, score) values (%s, %s)'
    cursor_in = db.cursor()
    cursor_in.execute(ins, [n, s])  # 可以在后面补位
    db.commit()
    cursor_in.close()
    db.close()

def fetchAll_data_mysql(table):
    conf = 'select * from %s'
    cursor_in = db.cursor()
    result = cursor_in.execute(conf, [table])
    # all = cursor_in.fetchall()
    print(result)
    db.commit()
    cursor_in.close()
    db.close()


# sql语句的参数化

# connect_mysql()
# insert_data_to_mysql("到底689687",20)
# fetchAll_data_mysql('t1')

class Mysqlpy:
    def __init__(self, databases, host="localhost",
                                  port="3306",
                                  user="root",
                                  password="12345678",
                                  charset="utf8"):

        self.databases = databases
        self.host = host
        self.port = port
        self.user = user
        self.password = password

    def open(self):
        self.db = pymysql.connect(host=self.host,database=self.databases,user=self.user,password=self.password)
        self.cursor = self.db.cursor()

    def exe(self, sql, L=[]):
        self.open()
        self.cursor.execute(sql,L)
        self.db.commit()
        self.close()
        pass

    def close(self):
        self.db.close()
        self.cursor.close()

    def fetchAll(self,sql,L=[]):
        self.open()
        self.cursor.execute(sql,L)
        result = self.cursor.fetchall()
        print("fetchAll",result)
        self.close()
        return result
        # pass


if __name__ == "__main__":
    sqlTest = Mysqlpy('db5')
    # ins = 'insert into t1(name,score) values (%s,%s)'
    # n = "dashen"
    # s = 80
    # sqlTest.exe(ins,[n,s])

    sel = "select * from t1"
    sqlTest.fetchAll(sel)

    pass


