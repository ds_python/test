import threading


class MyThread(threading.Thread):

    def __init__(self, times, *, info=""):
        super.__init__()

        self.times = times
        self.info = info

    def run(self):
        import time
        for i in range(self.times):
            print("线程信息", self.info, 'i=', i)
            time.sleep(1)


t1 = MyThread(10, info="线程1")
t2 = MyThread(5, info="线程2")
t1.start()
t2.start()

t1.join()
t2.join()


print("主线程退出!")
