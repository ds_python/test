# 协程，multi_task.py,两个任务是否可以同时执行
# 希望的执行结果是：1A2B3C4D
import sys
import time
from threading import Event

e1 = Event()  # 用于阻塞task1
e2 = Event()  # 用于阻塞task2

e2.clear()  # 重置为


def f():
    for x in range(1000000000):
        pass


def task():

    e2.set()
    e1.clear()
    e1.wait()

    time.sleep(1)
    print(1)
    time.sleep(1)

    print(2)
    time.sleep(1)

    print(3)
    time.sleep(1)

    print(4)


def task2():
    e2.wait()
    e1.set()
    e2.set()
    e1.wait()

    time.sleep(0.5)
    print("A")
    time.sleep(1)
    print("B")
    time.sleep(1)
    print("C")
    time.sleep(1)
    print("D")


f()

from multiprocessing import Process
from threading import Thread

# p1 = Process(target=task)
# p2 = Process(target=task2)

p1 = Thread(target=task)
p2 = Thread(target=task2)

p1.start()
p2.start()

p1.join()
p2.join()





task()
task2()





