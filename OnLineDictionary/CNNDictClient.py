# 词典客户端

import sys
from socket import *
import getpass

# 命令行传参 python3 127.0.0.1 8000 启动
# 不指定服务器地址多少，可以在启动的时候命令行输入

class CNNDictClient(object):
    """
    词典客户端对象
    1.展示功能菜单
    2.链接服务器
    """
    def __init__(self):
        self.name = ""


    def __str__(self):
        return "hello CNNDictClient!"


    def do_query(self, s, name):
        while True:
            word = input("单词:")
            if word == '##':
                break
            msg = "Q %s %s" % (name, word)
            s.send(msg.encode())
            data = s.recv(2048).decode()
            print(data)


    def do_hist(self, s, name):
        """
        查单词历史
        :param s:
        :param name:
        :return:
        """
        msg = "H %s" %name
        s.send(msg.encode())
        data = s.recv(128).decode()
        if data == 'OK':
            while True:
                data = s.recv(1024).decode()
                if data == '##':
                    print(data)
        else:
            print("没有历史记录")


    def show_function_menu(self, s, name):
        """
        展示功能菜单
        :param s:
        :return:
        """
        while True:
            print('''
                   ============ function ===========
                   --1.查词 2.历史 3.退出...
                   ================================
               ''')
            try:
                cmd = int(input("请输入选项>>"))

            except Exception as e:
                print("命令输入错误", e)
                continue

            if cmd not in [1, 2, 3]:
                print("请输入正确命令!")

            if cmd == 1:
                self.do_query(s, name)
            elif cmd == 2:
                self.do_hist(s,name)


    def show_menu(self, s):
        """
        展示菜单
        :return:
        """
        while True:
            print('''
                   ============ welcome ===========
                   --1.注册 2.登录 3.退出...
                   ================================
               ''')

            try:
                cmd = int(input("请输入选项>>"))

            except Exception as e:
                print("命令输入错误",e)
                continue

            if cmd not in [1,2,3]:
                print("请输入正确命令!")

            if cmd == 1:
                ''' 客户端注册 '''
                self.do_register(s)

            elif cmd == 2:
                ''' 登录 '''
                self.do_login(s)

            elif cmd == 3:
                s.send(b'E')
                s.close()  # 客户端自己端开链接
                sys.exit('谢谢使用')
            return

    def connect_server(self):
        """
        链接服务器
        :return:
        """
        if len(sys.argv) < 3:
            print("argv is error")

        HOST = sys.argv[1]
        PORT = int(sys.argv[2])

        s = socket()

        try:
            s.connect((HOST,PORT))

        except Exception as e:
            print(e)
            return

        self.show_menu(s)


    def do_login(self, s):

        name = input("User:")
        passwd = getpass.getpass()
        msg = 'L %s %s ' % (name, passwd)
        s.send(msg.encode())  # 发送登录指令，等待服务器相应
        data = s.recv(128).decode() # 等待服务器相应

        if data == 'OK':
            print("接收到服务器反馈的数据", data)
            # 进入下一级页面
            self.show_function_menu(s, name)
        else:
            print("登录失败！", data)


    def do_register(self, s):
        """
        发送注册指令
        :return:
        """
        while True:
            name = input("User:")
            passwd = getpass.getpass()
            passwd1 = getpass.getpass("Again:")
            msg = ''

            if (' ' in name ) or (' ' in passwd):  # 用户名、密码不能有空格
                print("用户名密码不许有空格")
                continue
            if passwd != passwd1:
                print("两次密码输入不一致")
                continue

            msg = "R %s %s " % (name,passwd)
            print("注册信息:%s" % msg)

            s.send(msg.encode())  # 发送注册指令，等待服务器相应

            data = s.recv(128).decode()

            if data == 'OK':
                # 注册成功
                self.show_function_menu(s, name)

            elif data == 'EXISTS':
                # 该用户已经注册
                # continue # 重新输入
                print("该用户已经注册")
                return # 返回一级命令
            else:
                print("注册失败！")
                return



if __name__ == '__main__':

    dict_client = CNNDictClient()
    dict_client.connect_server()
