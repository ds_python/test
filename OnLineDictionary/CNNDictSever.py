# 搭建通信服务
#

from socket import *
import pymysql
import os
import sys
import signal
import time

# from OnLineDictionary.CNNWordSettings import *
# from OnLineDictionary.CNNMySqlManage import *

from CNNWordSettings import *
from CNNMySqlManage import *
import threading

class CNNDictServer(object):
    def __init__(self):
        print("__init__ call")
        self.mysql_user = None

    def task_listen_client_thread(self, c, socketfd, db):
        print("task_listen_client_thread")

        t = threading.Thread(target=self.do_child_client_msg, args=(c, socketfd, db))  # 创建线程
        t.setDaemon(True)  # 设置为后台线程，这里默认是False，设置为True之后则主线程不用等待子线程
        t.start()  # 开启线程
        #self.do_child_client_msg(c, db)


    def task_listen_client_os_fork(self, c, socketfd, db):
        """os_fork 方式创建线程
        创建父子进程,解决并发问题
        :param c:
        :param socketfd:
        :return:
        """
        print("Before fork process pid=%s, ppid=%s" % (os.getpid(), os.getppid()))
        pid = os.fork()  # 开父子进程
        # mulprocess 创建的子进程不能用input
        if pid == 0:
            print("创建监听子进程...")
            print("I am child process pid=%s, ppid=%s" % (os.getpid(), os.getppid()))
            socketfd.close()  # 子进程s没用
            self.do_child_client_msg(c, db)
            sys.exit()  # 子进程退出,避免回到父进程while循环
        elif pid < 0:
            print("创建子进程失败")
        else:
            c.close()
            print("I am parent process pid=%s, ppid=%s" % (os.getpid(), os.getppid()))
        print("After fork process pid=%s, ppid=%s" % (os.getpid(), os.getppid()))


    def do_child_client_msg(self, c, db):
        """
        处理客户端请求
        :param c: 客户端socket链接
        :param db: 数据库
        :return:
        """
        print("do child! ...")
        while True:
            data = c.recv(128).decode()
            print(c.getpeername(), ":", data)
            print(data)

            if not data or data[0] == 'E':
                print("收到的数据异常")
                c.close() # 端开客户端链接
                sys.exit()
                break
            elif data[0] == 'L':
                self.do_login_to_server(c, data, db)
            elif data[0] == 'R':
                self.register_user_to_server(c, data ,db)
            elif data[0] == 'Q':
                self.do_query_from_txt(c, db, data)
            elif data[0] == 'H':
                self.do_hist_server(c, data, db)



    def do_hist_server(self, c, data, db):
        """
        服务器从数据库中读取到数据
        :param c:
        :param data:
        :param db:
        :return:
        """
        l = data.split(' ')
        name = l[1]
        cursor = db.cursor()

        sql = "select * from t_history where name='%s' limit 10" % name
        cursor.execute(sql)
        r = cursor.fetchall()

        if not r:
            c.send(b'Fail')
        else:
            c.send(b'OK')
            time.sleep(0.1)

        for i in r:
            msg = "%s %s %s " % (i[1], i[2], i[3])
            c.send(msg.encode())
        time.sleep(0.1)

        c.send(b'##')


    def do_login_to_server(self, c, data, db):
        """
        用户登录服务器
        :param c:
        :param data:
        :param db:
        :return:
        """
        print("do_login_to_server")

        name, passwd = self.parse_register_data_from_client(data)
        cursor = db.cursor()

        sql = "select * from t_user where username= '%s' and password='%s'" %(name, passwd)
        cursor.execute(sql)
        r = cursor.fetchone()
        print(r)

        if r == None:
            c.send(b'Fail')
        else:
            c.send(b'OK')



    def do_query_from_txt(self, c, db, data):
        """
        服务器查询单词
        :param c:
        :param db:
        :param data:
        :return:
        """
        print("do_query_from_txt", data)

        l = data.split(' ')
        name = l[1]
        word = l[2]
        cursor = db.cursor()
        tm = time.ctime()

        sql = "insert into t_history (name, word, time) values ('%s', '%s', '%s')" %(name,word,tm)

        try:
            print("insert query word history: %s" % sql)
            cursor.execute(sql)
            db.commit()  # 避免保存不上

        except Exception as e:
            print(e)

        # 使用文本查找
        with open(DICTIONARY_FILE_PATH + "dict.txt") as f:
            for line in f:
                tmp = line.split(' ')[0]
                if tmp > word:
                    break
                elif tmp == word:
                    # c.send(b'OK')
                    print("文本发现单词：%s ,desp: %s" % (word, line.encode()))
                    c.send(line.encode())
                    return
                else:
                    pass

            c.send("没有该单词".encode())
            f.close()


    def check_user_from_mysql(self, db, name):
        """
        检查用户是否在表中存在
        :return:
        """
        # 判断数据库是否已经存在这条数据
        fetch_sql = "select * from t_user where username = '%s'" % name
        print(fetch_sql)

        cursor = db.cursor()
        cursor.execute(fetch_sql)
        r = cursor.fetchone()
        print(r)
        db.commit()

        return r

    def register_user_to_server(self, c, data, db):
        """
        向服务器注册用户数据
        :param c: 
        :param data: 
        :param db: 
        :return: 
        """
        name, passwd = self.parse_register_data_from_client(data)
        print("%s 来注册了" % name)

        r = self.check_user_from_mysql(db, name)

        if r != None:
            c.send(b'EXISTS')
            return
        
        sql = "insert into t_user (username, password, email) values ('%s', '%s', '%s')" % (name, passwd, '')
        print(sql)
        # user = TUser(name, passwd, '')
        try:
            # db为数据库
            # cursor.execute(sql, [user.name, user.password, ''])
            cursor = db.cursor()
            cursor.execute(sql)
            db.commit()

            # self.mysql_user.insert_user_to_mysql(sql, user)
            # self.mysql_user.close() # 关闭数据库

            c.send(b'OK')  # 告诉客户端注册成功
            
        except Exception as e:
            print(e)
            db.rollback()
            c.send(b'Fail')

        
    def parse_register_data_from_client(self, data):
        # 解析data
        l = data.split(' ')
        name = l[1]
        passwd = l[2]
        return name,passwd

    def fetch_user_from_db(self):
        # 查询数据
        pass


    def client_connect(self, s, db):
        """
        处理客户端链接
        :return:
        """
        signal.signal(signal.SIGCHLD, signal.SIG_IGN)

        while True:
            try:
                c, addr = s.accept()
                print("Connect from", addr)
            except KeyboardInterrupt as e:
                print(e)
                s.close()
                sys.exit("退出进程!")
            except Exception as e:
                print(e)
                continue

            # 这个地方的监听可能会受到上面影响
            # self.task_listen_client_thread(c, s, db)
            self.task_listen_client_os_fork(c, s, db)


    def create_socket(self, db):

        s = socket()
        s.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
        s.bind(ADDR)
        s.listen(5)
        print("Listen the port 8000...")

        self.client_connect(s, db)


    def main(self):

        mysql_c = CNNMySqlManage('db_word')
        db_word, curse = mysql_c.open()
        self.mysql_user = mysql_c

        self.create_socket(db_word)



if __name__ == '__main__':
    server = CNNDictServer()
    server.main()
