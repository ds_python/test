from OnLineDictionary.CNNDictClient import *

class CNNDictClientFunction(CNNDictClient):
    def __init__(self):
        pass

    def do_query(self, s, name):
        word = input("单词:")
        while True:
            if word == '##':
                break
            msg = "Q %s %s" %(name, word)

        s.send(msg.encode())
        data = s.recv(128).decode()
        if data == 'ok':
            data.recv(2048).decode()
            print(data)
        else:
            print(data)

    def do_hist(self, s, name):
        """
        查单词历史
        :param s:
        :param name:
        :return:
        """
        msg = "H %s" % name
        s.send(msg.encode())
        data = s.recv(128).decode()
        if data == 'OK':
            while True:
                data = s.recv(1024).decode()
                if data == '##':
                    print(data)
        else:
            print("没有历史记录")


if __name__ == '__main__':

    dict_function = CNNDictClientFunction()

