import pymysql

# from OnLineDictionary.Model.TUser import *
from Model.TUser import *

class CNNMySqlManage(object):
    """
        定义数据库
    """
    def __init__(self, databases, host="localhost",
                                  port="3306",
                                  user="root",
                                  password="12345678",
                                  charset="utf8"):

        self.databases = databases
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.db = None
        self.cursor = None

    def open(self):
        """
        建立数据库连接
        :return:
        """
        self.db = pymysql.connect(host=self.host,database=self.databases,user=self.user,password=self.password)
        self.cursor = self.db.cursor()

        # print(self.db, self.cursor)
        print("数据库打开成功")
        return self.db, self.cursor


    def insert_user_to_mysql(self,ins_sql, user):
        """
        把用户数据插入到表中
        :param ins_sql: 插入语句
        :param user: 用户实体类
        :return:
        """
        cursor_in = self.db.cursor()
        print("insert_user_to_mysql", ins_sql)
        cursor_in.execute(ins_sql, [user.name, user.password, ''])  # 可以在后面补位
        self.db.commit()

    def insert_data_to_mysql(self, ins_sql, dict=None):
        """
        插入数据到表中
        :param dict 需要存储的数据
        :return:
        """
        print(ins_sql)
        cursor_in = self.db.cursor()
        cursor_in.execute(ins_sql, [dict['word'], dict['content'],''])  # 可以在后面补位
        self.db.commit()

        # cursor_in.close()
        # db.close()

    def exe(self, sql, L=[]):
        self.open()
        self.cursor.execute(sql,L)
        self.db.commit()
        self.close()
        pass

    def close(self):
        self.db.close()
        self.cursor.close()

    def fetchAll(self,sql,L=[]):
        self.open()
        self.cursor.execute(sql,L)
        result = self.cursor.fetchall()
        print("fetchAll",result)
        self.close()
        return result


if __name__ == '__main__':

    mysql_client = CNNMySqlManage('db_word')
    mysql_client.open()  # 打开数据库

