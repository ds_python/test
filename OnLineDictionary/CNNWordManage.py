# 单词本处理
# 优先处理单词
#
from OnLineDictionary.CNNWordSettings import *
from OnLineDictionary.CNNMySqlManage import *

def readDictFile(name):
    """
    读取单词本文件
    :param path: 单词本路径
    :return:
    """
    filename = DICTIONARY_FILE_PATH + name
    with open(filename) as f:
        line = f.readline()  # 调用文件的 readline()方法
        while line:
            #print(line, end = '')
            line = f.readline()  # 单词本逐行读取
            dict = deal_word_line(line)
            #print(t)
            save_mysql(dict) # 保存单词到数据库
    f.close()


def deal_word_line(line):
    """
    单词按行解析
    :param line:
    :return:
    """
    word = line[0:16].strip()
    content = line[17:]
    dict = {'word': word,'content': content}
    #print(dict)
    return dict

def save_mysql(dict):
    """
    单词按行保存到数据库
    :param dict:
    :return:
    """
    word = dict['word']
    content = dict['content']
    sql = 'insert into t_word (word, description, extra) values (%s, %s, %s)'
    print("sql: %s" % sql)

    try:
        # 执行插入数据
        mysql_client.insert_data_to_mysql(sql,dict)
        # mysql_client.close()

    except Exception as e:
        print(e)

    return mysql_client



if __name__ == '__main__':

    # 初始化数据库
    mysql_client = CNNMySqlManage('db_word')
    mysql_client.open()  # 打开数据库

    # 解析单词本
    readDictFile('dict.txt')
    #save_mysql({})

    #test_filter()