
pymongo python 操作mongodb

1.文件存储,blob 可以存文件
 数据库中文件存储方式
 -存路径，文件存放到磁盘中
 优点，处理简单，不会占用服务器存储空间
 缺点：当路径和实际位置不一致时，找不到文件
 -直接存将文件存储到数据库
 优点：不会出现路径和文件不一致
 缺点：占用数据库存储空间
 
2.GridFS村粗文件的方式
-主要存储二进制文件（图片、视频、）
-存储原来，将文件分解成很多片段 (chunk)
每个chunk作为一个文档存储chunks集合中
-优点，能够自动同步文件元数据读取文件，只需要加载部分数据即可
-读取效率比直接从磁盘读取要慢
-如果文件需要替换，只能整体替换，整体更新

示例：GridFS存储文件
mongofiles 命令存储文件,一个文件分片的大小 255k

```
mongofiles -d gridfs put /Users/dushuzhong/Pictures/1539138843717.jpg
```

这个命令会生成2个文件，
md5摘要信息比较这个文件是否被篡改

确保文件没有被串改过

提取图片
```
mongofiles -d gridfs get ~/1539138843717.jpg
```

mongofiles -d gridfs get ~/Desktop/1539138843717.jpg

使用python 操作mongodb

1.安装 pip3 install pymongo

2.访问mongodb的链接
    2.1 创建mongoDB服务器的链接
    2.2 获取数据库对象
    2.3 通过数据库对象获取集合对象
    2.4 执行操作
    2.5 关闭数据链接
    
示例1 显示服务器上所有的库
    show_dbs.py
    
示例2 插入
    -insert() 插入一笔或者多笔
    -insert_one() 插入一笔
    -insert_many() 插入多笔
    -save() 如果_id存在则更新，否则插入

*以上几个函数，均传入一个字典类型的参数

update{qyery,update,upsert=False,mutli=True}
upsert 是否执行插入
multi 是否修改多笔











