# 进程间通信

进程间通信 IPC(Inter-Process Communication)
什么是进程间通信
  进程空间相对独立，资源无法相互获取，此时不同进程间传递信息需要进程间通信

进程间通信的示例见:
no_ipc.py

>管道 Pipe 一端写，一端读取。都是从缓存区读和取 \n
>消息队列 \n
>共享内存 \n
>信号量 \n
>套接字（通常本地，远程的也可以）\n
...


##### 管道原理 Pipe
    原理：在内核中开辟管道空间，生成多个管道对象，多个进程 （通常只是两个进程）进行读写操作，来实现通信
    创建管道
        函数:
        multiprocessing.Pipe(duplex=True)
        作用:
        创建管道，用于两个进程间通信
        参数:
        duplex:
            True 建立双向通道（默认）
            False 建立单向通道
        返回值:
        (con1,conn2) 一对连接管道两端的IO对象，如果是双向管道，两端都可以读写，如果是单项管道，conn1只用来接受数据，conn2用来发送数据

    管道两端收发数据的方法：
        接收方法：
            conn.recv()
            返回值:读取的内容（可以写入Python内建的数据类型）
        发送方法：
            conn.send()
        作用：向管道写入内容

        管道的关闭方法:
            conn.close()
        作用:
            关闭连接的IO对象，释放管道资源


    管道的示例:
    pipe.py

    说明:
        管道一旦进行进程分裂，则父进程的两个IO对象conn1,conn2也同时会复制在子进程中，此管道的内核的引用计数会增加，此时需要父进程和子进程都关闭两个IO对象（即关闭4次）
        IO连接对象会复制一份，数据在内核中不会复制2份

    通信都要进过内核
    repr('hello'),可以将对象转换成字符串
    eval 可以把字符串直接转成列表主要做通信用

    b = b'[1, 2, 3, "abc"]'
    s = b.decode()


##### 消息队列 Message Quene
    队列的概念：
    先进先出 （FIFO,first in first out）
        原理：在内核内建立队列的内存结构，一个进程向里存入，另一个进程按先后循序取出（类似于UDP传输）
        类:
            multiprocessing.Quene()

        创建消息队列的构造函数：
        函数:
            multipleprocessing.Quene(maxsize=0)
        作用:
            创建消息队列对象
        参数:
            maxSize 表示队列中最多存放多少个消息（默认不受限制）
        返回值:
            消息队列对象
        向消息队列存入消息数据
            方法:
                quene.put(data, block=True, timeout=None)
            参数:
                data用来存储内容(python数据类型)
                block 默认队列满时会阻塞，设置为False则为非阻塞
                timeout 超时检测时间

            说明:
                当非阻塞put失败，或阻塞时timeout为正数且超时，则会触发quene.Full类型的错误

        多消息队列中提取消息
            方法:
                quene.get(block=True, timeout=None)
            参数:
                同quene.put中的参数规则相同
            说明:
                当非阻塞get失败或阻塞起时会触发quene.Empty错误

        其他方法:
            quene.full() 判断队列是否为满
            quene.empty() 判断队列是否为空
            q.size() 返回队列中的消息个数
            q.close() 关闭消息队列


        示例见
        quene.py


##### 共享内存 shared memory
    一段内存两个或两个以上的进程同时可见

    原理：
        在操作系统中开辟一段以字节为单位的内存空间，这段内存的地址会分别映射到不同的进程空间中，此段内存中对多个进程同时可见
    优点：
        速度快
    缺点:
        多个进程可同时操作这段内存，每次写入的内容会覆盖之前写入的内容，通常需要用互斥机制来解决冲突问题

    python 中两种共享内存的方式
        共享值
        共享数组

    共享值
        创建一个数据共享对象，来共享正数或浮点数
        创建共享值对象

        函数:
            multiprocessing.Value(typecode_or_type, Value)
        作用:
            开辟共享内存空间，用来存单个数字
        参数:
            typecode_or_type
                ctype类型或对应的字符串。表示共享内存中要存储的数据类型
                类型码 'i' ---> ctype.c_int (最小2字节) ---> int
                类型码 'f' ---> ctype.c_float (4字节) ---> float
                类型码 'c' ---> ctype.c_char (1字节) ---> char
            value
                初始值，整数或浮点数（受限制，max 65535）
        返回值:
            共享内存对象


    值对应的属性
        obj.value
        对改属性的修改和使用即对共享内存的数据进行修改和访问
     示例见:
    shm_value




    共享数组
        创建多个数据共享对象来共享多个同样的整数和浮点数
        创建共享数组对象

        函数:
            multiprocessing.Array(typecode_or_type, size_or_initializer)
        作用:
            开辟一段连续的共享内存空间来存放数据
        参数:
            typecode_or_type 同value共享值对象
            size_or_initializer
                整数 代表数字的个数 （默认值是0 或0.0）
                可迭代对象 提供多个数来初始化共享数组
        返回:
            共享值对象
        说明:
            共享数组自带进程锁，可以用with语句进行同步和互斥

    共享数组对象的属性
        obj.value
            数组对应的字节串或列表

        raw
            数组对应的原始数据的字节串（或列表）

        索引取值或切片
            len(obj) 得到共享数组的长度
            共享数组是可迭代对象

    示例见:
    shm_array.py



    互斥:
    互斥解决共享冲突的方式,

    同步:
        两个或者两个以上的进程或线程协调工作
    多进程锁 multiprocessing.Lock
        对多个进程中共享资源进行互斥操作

    创建多进程锁
        函数:
            multiprocessing.Lock()
        作用:
            对共享资源进行互斥操作
        返回值:
            多进程对象
        说明:
            需要用with语句进行加锁和解锁操作

        示例见:
        shm_value_lock.py


        加锁时间越短越好


##### 信号量（也叫信号灯）Semaphore
    作用:
    让多个进程协向工作
    原理:
        给定一个变量是一，多个进程同时可见，多个进程可以通过方法操作数量，达到协同工作目的
    类:
        multiprocessing.Semaphore

    创建:
    构造函数
        multiprocessing.Semaphore([Value])
    返回值:
        信号量对象

    申请获取信号量
    方法:
        sem.acquire()
    说明:
        将信号量减1，当信号量减后结果为负数时，该方法阻塞释放信号量

    释放信号量
    方法:
        sem.release()
    作用:
        将信号量+1（同时可能唤醒等待的信号量的进程）

    得到信号量值得方法
    方法:
        sem.get_value()
    作用:
        获取信号量的数量
    说明:
        在mac os x上没有此方法

    信号量说明
        可以用**with**语句获取信号量，离开with语句时，信号量会自动释放
        当初始信号量值为1时，用with语句能够实现多进程锁

    示例见:
    shm_value_semaphore.py  # 实现互斥

    一个进程分为
    text 代码段，执行指令
    data 全局变量
    栈 放的是局部变量
    堆 动态的内存对象


    多任务编程之线程 Thread

    什么是线程：
    1.线程是多任务的编程方法
    2.线程被称为是轻量级的进程
    3.线程是系统分配CPU资源的最小工作单位


    线程的特点：
    1.一个进程可以包含多个线程
    2.多个线程的执行循序互不影响
    3.线程的创建和销毁消耗的资源，远远低于进程
    4.线程共享进程中的内存资源

    说明：
    一台计算机可以有多个进程，一个进程可以有多个线程

    进程是管理资源的最小单位

    创建线程
    模块:
        threading
    函数:
        threading.Thread(target=None, name=None,args=(), kwargs={})
    作用:
        创建线程对象，只复制局部变量8M大小
    参数:
        target 绑定线程处理函数
        name 线程名称，默认为Thread -1
        args 元组, 给target线程函数按位置传参
        kwargs 字典，给target线程函数按关键词传参
    返回值:
        线程对象

    启动线程的方法:
    方法名:
        t.start()
    作用:
        启动线程，运行线程函数

    回收子线程资源
    方法名:
        t.join([timeout])
    作用:
        阻塞等待回收线程资源


    线程对象的属性
        t.name 线程的名字
    线程的方法
        t.setName(s) 设置线程的名称
        t.getName() 得到线程名称
        t.is_alive()
        获取线程的状态，如果已经结束则返回False
        否则返回True


    创建自己的线程类的步骤，Thread类
    1.继承自threading.Thread
    2.添加自己的初始化方法，用super调用父类的__init__方法
    3.覆盖run方法
    4.使用自己的类来创建线程对象，调用start启动线程来运行线程

    示例见:
        mythread.py

    线程间不是独立的

    示例见
        thread_communication.py


    线程的互斥
    多线程编程时注意事项：
        共享资源的争夺，需要使用互斥机制
        共享资源（临界资源）：多个线程可以使用操作的资源为共享资源
        临界区：指多个线程可能同时操作同一共享资源的代码

    互斥:
        互斥是一种制约关系，当一个进程或者线程使用临界资源时都按照规则拿到锁后在对临界资源进行操作，操作完毕后立即释放销毁的规则，可以避免公有资源的访问冲突

    示例见:
        thread_no_lock.py

    创建线程锁
        threading.Lock()

    上锁:
        lock.acquire()
    解锁:
        lock.release()


    死锁：
    多个线程同时在等


    线程的同步，就是协同步调，按预定的先后次序执行

    线程事件，内置一个标志。初始化值是False,当此标志为False时，线程用e.wait()方法进入阻塞等待，当有其他线程设置为True时，等待的线程才能执行

    创建线程事件对象
    函数:
        threading.Event()
    返回:
        线程事件对象

    线程同步等待的方法:
        方法名:
            e.wait([timeout])
        功能:
            阻塞函数，如果事件为False,时等待事件被设置为True,否则不进行等待
        参数:
            timeout 超时时间（默认无限等待）

    线程事件的其他方法
        e.set() 将线程事件设置为True状态
        e.clear() 将线程事件设置为False状态
        e.is_set() 判断当前事件状态是True还是False


    python线程的GIL问题
        GIL （Global Interpreter Lock）
        全局解释器锁

        由于python解释器设置中加入了全局解释器锁，导致python解释器在同意时刻只能解释一个线程，所以大大降低了python的执行效率
        说明:t
            python线程一般用在大量阻塞IO的程序，或者延迟IO程序中（网络消息发送），因为python线程在遇到阻塞时主动，让出GIL
        CIL建议：
            尽量使用进程完全并发
            不使用CPython 作为解释执行器，改为Jython,或IronPython
        可以使用多种组合解决并发问题


        示例见:
            thread_GIL.py


    实现自动对话机器人服务器
    发送：xxx
    返回：====xxx====

    服务器端能统计用户在线人数
    1.IO多路复用 select实现 (单进程，单线程)
    2.改为多进程实现
    3.改为多线程实现


    练习:
        1.将robot_server_process.py,用进程间通信，实现主进程知道在线人数，并显示在线人数:
        2.实现子进程资源回收


    创建自己的线程类
    class MyThread(threading.Thread):
        def __init__(self):
            super().__init__()  # 初始化父类

        def run():


    协程 coroutine
        又称微线程，协程是为非抢占式多任务产生子程序的计算机程序组件
        协程允许不同入口点在不同位置暂停或者开始，简单来说，协程是可以暂停执行并能恢复执行的函数任务
        协程是合作式多任务的实现方式

        多任务示例见：
        multi_task.py


    python 的协程的实现原理：
        python的协程是用生成器函数实现的


    含有yield的语句的函数是生成器
        yield
        示例见：
        coroutine_yield.py

    python协程的任务函数的特点:
        1.任务函数内可以用局部变量存放当前任务信息，在协程函数退出前不会被释放
        2.协程函数不同于进程和线程吗，它不会因cpu,调度而自动切换，需要某种机制手动切换
        3，协程函数调度切换时，将记录的上下文，保存起来，在切换回来时进行调度，回复原有的执行内容，因此可以从上一次执行的位置继承执行

    协程的优缺点：
        优点：
            协程是一个单线程程序，占有计算机资源很少
            协程间切换的开销少，执行效率高
            协程无需同步互斥

        缺点：
            无法利用计算机多核资源
            如果一个协程内出现死循环且没有让出协程的控制权，则其他协程将无法得到执行

    python协程的调度与通信
        协程函数的传送数据方法：
            co.send(value)
        参数:
            value 传参给生成器的yield的表达式的值，第一次调用此方法时参数必须设置为None
        返回值:
            生成器内下一次yield 表达式的值，当生成器函数调用结束时会触发StopIteration异常
        作用:
            恢复执行生成器函数，value参数将作为生成器函数yield表达式的返回值，当此函数返回，说明生成器函数已经执行到下一个yield表达式并挂起生成器函数执行并返回

        yield 表达式
            语法:
            r = yield [表达式]
            说明:
               当表达式省略时，等同于 yield None

        协程之 "greenlet"，协程服务的三方库

        函数：
            greenlet.greenlet(task_fun)

        参数：
            task_fun 协程任务函数

        切换协程任务
            方法:
                greenlet.switch()
            功能:
                选择要执行的协程事件
            说明:
                第一次调用时的传参时传给协程函数的形参，必须和协程函数的调用一致

            示例见:
                coroutine_greenlet.py


            协程之 gevent
                安装：
                    sudo pip3 install gevent



            创建协程任务:
                函数:
                    gevent.spawn(function, *args, **kwargs)

                参数:
                    function 协程任务函数
                    args 给协程函数位置传参数
                    kwargs 给协程函数关键字传参数

                返回值:
                    返回greenlet协程对象


            运行协程任务，协程资源回收
                函数：
                    gevent.joinall(greenlet, timeout=None)
                参数:
                    greenlet 要回收的协程列表
                    timeout 浮点数,超时时间(秒为单位)

                返回值:
                    返回在超时之前已经运行完毕的greenlet协程对象组件的列表
                    此greenlet对象也可以调用g.get() 方法获取协程的运行结果

                功能:
                    运行协程任务，阻塞等待回收协程


            gevent 创建协程的步骤
                1.将协程事件封装为任务函数
                2.用gevent.spawn生成协程对象（greenlet对象）
                3.用event.joinall 函数运行协程函数，执行协程任务

            用阻塞和拿书切换协程
                睡眠函数
                    gevent.sleep(second=0)
                参数:
                    阻塞时间（秒）
                作用:
                    让gevent 当前协程阻塞，同时切换到其他协程
                示例见:
                    coroutertine.py


        HTTP协议的WEB应用
            HTTP 超文本传输协议
            应用层协议，默认端口号：80 端口
            作用:
                网页的数据获取和传输

            特点:
               1.应用层协议，传输层选择TP传输
               2.简单，灵活，很多语句都有Http专有接口
               3.无状态协议，协议提身不要求记录传输的数据
               4.http1.1 支持持久连接

            请求过程
               1.客户端发出请求给服务器
               2.服务器返回响应给客户端


        Http请求request格式：
            1.请求头
                1）请求行
                {方法} SP {url} HTTP/1.x CRLF
                如 GET/index.html HTTP/1.1
                2) 实体头
                {类型名}:SP {值} CRLF
                如：
                    Host:localhost:8080
                    Connection: keep-alive
                3) 关部结束标志
                CRLF 即 b'\r\n'

            2.请求体(HTTP数据)

        http响应 response 格式
        GET /index.html HTTP/1.1 <回车换行'\r\n'>
        键:值<回车换行>
        <回车换行>
        数据

        1.响应头 resposne格式
            1.响应头
                1） 响应行
                    Http 1.x SP {状态码} SP {描述} CRlf
                    如：
                        HTTP/1.1 200 OK
                        HTTP/1.1 404 Not Found
                2) 实体头：
                    （类型名）：SP {值} CRLF
                3) 头部结束标识

            2.响应体
            http响应数据
            响应头的状态码：
            状态码：
            值:
                1xx 提示信息，表示请求被接收
                2xx 响应成功
                3xx 响应需进一步操作，重定向
                4xx 客户端错误
                5xx 服务端错误


            常用:
                200 成功
                404 访问内容不存在
                401 没有访问权限

        示例见：
            http_server_select.py

































