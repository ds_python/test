from multiprocessing import Process
from multiprocessing import Pipe
import os
import time

# 创建管道
conn1, conn2 = Pipe()  # 默认建立一个双向管道


def task_func():
    '''
        子进程任务处理函数，
        用conn1接收父进程的数据
        用conn2来发送数据给父进程
    :param a:
    :param b:
    :param c:
    :return:
    '''
    print("child is runing...")
    while True:
        obj = conn1.recv()  # 从conn1接收数据
        print(os.getppid(), "收到信息", obj)
        if obj == 'exit':
            break  # 让进程函数结束退出子进程

        send_obj = '====' + obj + '===='
        # 讲send_obj 送回给主进程
        conn1.send(send_obj)

    conn1.close()
    conn2.close()  # 关闭管道


p = Process(target=task_func)
p.start()

# 主进程做的事
conn1.close()
print("father processs is runing...", flush=True)
while True:
    s = input("请输入发送给子进程的数据:")
    conn2.send(s)
    if s == 'exit':
        break
    obj = conn2.recv()
    print("子进程给父进程发送的数据是:", obj)
conn2.close()
# conn1.close()


p.join()
print("程序正常退出")
