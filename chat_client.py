from socket import  *
import os, sys

addr = ('127.0.0.1', 8888)

# 客户端随时发消息，随时收消息
def send_msg(s, name):
    while True:
        try:
            text = input("发言:")
        except KeyboardInterrupt:
            text = 'quit'

        if text == 'quit':
            msg = "Q " + name
            s.sendto(msg.encode(), addr)
            sys.exit("退出聊天室")

        msg = "C %s %s" %(name, text)
        s.sendto(msg.encode(), addr)


def recv_msg(s):
    """
        接收服务转发过来的消息
    :param s:
    :return:
    """
    while True:
        data, addr_tmp = s.recvfrom(2048)
        print(data.decode() + '\n' + "发言:",end='')

        if data.decode == 'EXIT':
            sys.exit("")
        pass

# 创建套接字
def main():
    s = socket(AF_INET, SOCK_DGRAM)
    # 发送请求进入聊天室, 定义一个比较简单的协议
    while True:
        name = input("请输入姓名：")
        msg = 'L ' + name
        s.sendto(msg.encode(), addr)
        # 等待服务器反馈
        data, addr_tmp = s.recvfrom(1024)
        if data.decode() == 'OK':
            print("已经进入聊天室")
            break
        else:
            print("不允许进入的原因", data.decode())

    #s.sendto(b'zhangsan', addr_tmp)
    # 创建父子进程
    pid = os.fork()
    if pid < 0:
        print("Process error!")
    elif pid == 0:
        send_msg(s, name)
        pass
    else:
        recv_msg(s)
        pass


if __name__ == '__main__':
    main()