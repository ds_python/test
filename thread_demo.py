import time
from threading import Thread


def thread_task1():
    for i in range(10):
        print("子线程中的i=", i)
        time.sleep(2)
    print("子线程1执行结束")


t = Thread(target=thread_task1)
t.start()
t.setName("mythread")
print("threadName:", t.getName())

def main_task():
    for i in range(15):
        print("主线程中的i=", i)
        time.sleep(1)
    print("-------主线程执行结束--------")


main_task()
print("子线程的状态是", t.is_alive())
t.join()  # 会等子线程

print("子线程的状态是", t.is_alive())

# 其实是进程中的线程来执行函数资源的

