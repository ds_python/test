# http://www.kugou.com/yy/rank/home/1-8888.html 抓取kugou Top500

# 网络请求库
import requests
# 可以快速解析网页
from bs4 import BeautifulSoup
# 正则自带库
import re
import time
from pymongo import MongoClient

from lxml import etree

# 爬虫

headers = {
    'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'
    }

a = 'xxlxxjshiojodjxxlovexxwiodiaodwji'
infos = re.findall('xx(.*?)xx',a)
print(infos)


b = 'one1two2three3'
b_infos = re.search('\d+',b)
b_infos_new = re.findall('\d+',b)
print(b_infos_new)
c = '185-17078-0940'
#newC = re.sub('\D',c)

#print(newC)


print(b_infos)

client = MongoClient('localhost',27017)

mydb = client.local
test = mydb.test


def get_info(url):

    wb_data = requests.get(url,headers = headers)
    soup = BeautifulSoup(wb_data.text, 'lxml')
    ranks = soup.select('span.pc_temp_num')
    titles = soup.select('div.pc_temp_songlist > ul > li > a')
    times = soup.select('span.pc_temp_tips_r > span')

    # 可以使用re.find_all来匹配标签，提取数据
    for rank,title,time in zip(ranks,titles,times):
        data = {
            'rank':rank.get_text().strip(),
            'singer':title.get_text().split('-')[0],
            'song':title.get_text().split('-')[1],
            'time':time.get_text().strip()
        }
        print(data)

        # 把数据插入到mongo
        #test.insert({'title': data['title'], 'address': data['address'], 'price': 'Cleveland', 'img': data['img'],
        #             'name': data['name'], 'sex': data['sex']})


if __name__ == '__main__':

    urls = ['http://www.kugou.com/yy/rank/home/{}-8888.html'.format(str(i) for i in range(1,24))]
    for url in urls:
        get_info(url)
        time.sleep(1)
