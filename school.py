from  urllib import request #导入urllib库中的request方法
import re

def focus_newslist():
    focusnewsUrl = 'http://xxx.xxx.xxx.htm' #要闻的链接
    with request.urlopen(focusnewsUrl) as f:
        print('Status:',f.status,f.reason)
        for k, v in f.getheaders():
            print('%s:%s' % (k,v))
        dataAll=f.read()
        # 使用正则表达式，提取新闻列表ul标签下内容
        data_ul=re.findall('<div.?class="classcontent">.*?(<ul>.*?<li.?class=".*?"><a.?href=".*?".?title=".*?".?target="_blank">.*?</a><em>.*?</em></li>.*?<span>.*?</span>.*?</ul>).*?</div>',dataAll.decode('utf-8'),re.M|re.S)
        for datas in data_ul:
            i=datas
        matchdata=re.findall('<li.*?class=".*?"><a.?href="(\..*?)".?title=".*?".?target="_blank">(.*?)</a><em>(.*?)</em></li>.*?<span>(.*?)</span>',i,re.S)
        items=[] # 使用列表来存储，抽取出来的内容
        for item in matchdata:
            # 使用字典来讲抽取的内容，填入列表中
            items.append({'url':item[0].replace("\n","").strip('.'),
                          'title':item[1].replace("&nbsp;","").strip('\r\n'),
                          'time':item[2].replace("\n","").strip('()'),
                          'content':item[3].replace("\r\n","").strip('&nbsp;')})
        for content in items:
            print(content) # 打印内容