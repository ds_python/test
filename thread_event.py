from threading import Event
from threading import Thread
import time

event = Event()


def thread_task(info):
    print(info, "进入等待...")
    event.wait()
    print(info, "解除等待!!! ")
    # event.clear()


t1 = Thread(target=thread_task, args=("线程1",))
t2 = Thread(target=thread_task, args=("线程2",))

t1.start()
t2.start()

for i in range(10):
    time.sleep(5)
    print("主进程将事件设置为True")
    event.set()
    time.sleep(3)
    event.clear()

t1.join()
t2.join()
print("========= 主线程结束 =========")
