class Human:
    def __init__(self,n,a):
        self.name = n  # 姓名
        self.age = a  # 年龄
        self.money = 0  # 钱数
        self.skill = []  # 用来保存每个对象的技能

    def teach(self, othter, skill_name):
        print(self.name, "教", othter.name, skill_name)
        # 技能列表
        othter.skill.append(skill_name)

    def work(self, m):
        print(self.name, "赚了", self.money, "元钱")
        self.money += m

    def borrow(self, other, m):
        if other.money > m:
            print(self.name, "向", other.name, "借了", m, "元钱")
            self.money += m
            other.money -= m
            return True
        else:
            print(self.name, "向", other.name, "借钱失败")
            return False

    def show_info(self):
        print(self.age, "岁的", self.name, "有钱", self.money, "元,他学会的技能是", "、".join())

# 对象自身的实例变量里面
# 少用全局变量
# 属性，行为

zhang3 = Human("张三", 35)
li4 = Human("李四", 15)

zhang3.teach(li4, "python")
li4.teach(zhang3, "跳皮筋")

zhang3.work(1000)
zhang3.borrow(li4, 1000)

zhang3.show_info()


#print(zhang3)
#print(li4)










