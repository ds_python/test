import requests
from bs4 import BeautifulSoup
import os
import sys


##############  linux 平台测试没有问题   ###############

#
# if(os.name == 'nt'):
#         print(u'你正在使用win平台')
# else:
#         print(u'你正在使用linux平台')

#这个根据系统自己设置
header = {'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',

          }
#http请求头
all_url = 'http://www.mzitu.com'

start_html = requests.get(all_url,headers = header)

#保存地址
path = '/Users/dushuzhong/Desktop/unrar_books/'

#找寻最大页数
soup = BeautifulSoup(start_html.text,"html.parser")
page = soup.find_all('a',class_='page-numbers')
max_page = page[-2].text

same_url = 'http://www.mzitu.com/page/'

def addRefer(pic_refer):
    header['Referer'] = pic_refer[0]['href']

def main_scrap():
    """
        开始爬取你需要的图片资源
    :return:
    """
    for n in range(1, int(max_page) + 1):
        '''
            构造url
        '''
        ul = same_url + str(n)
        start_html = requests.get(ul, headers=header)
        soup = BeautifulSoup(start_html.text, "html.parser")
        all_a = soup.find('div', class_='postlist').find_all('a', target='_blank')
        for a in all_a:
            ''' 
                #解析网页，提取需要的文本
            '''
            title = a.get_text()
            if (title != ''):
                print("准备扒取：" + title)

                # win不能创建带？的目录
                if (os.path.exists(path + title.strip().replace('?', ''))):
                    # print('目录已存在')
                    flag = 1
                else:
                    os.makedirs(path + title.strip().replace('?', ''))
                    flag = 0
                os.chdir(path + title.strip().replace('?', ''))
                href = a['href']
                html = requests.get(href, headers=header)
                mess = BeautifulSoup(html.text, "html.parser")
                pic_max = mess.find_all('span')
                pic_max = pic_max[10].text  # 最大页数

                if (flag == 1 and len(os.listdir(path + title.strip().replace('?', ''))) >= int(pic_max)):
                    print('已经保存完毕，跳过')
                    continue
                for num in range(1, int(pic_max) + 1):

                    pic = href + '/' + str(num)

                    html = requests.get(pic, headers=header)
                    mess = BeautifulSoup(html.text, "html.parser")
                    pic_url = mess.find('img', alt=title)

                    pic_refer = mess.select('div.main-image > p > a')

                    try:
                        # http://i.meizitu.net/2018/08/20e03.jpg
                        # print("download_url:",pic_url['src'])

                        # header['Referer'] = pic_refer['href']
                        # print("pic_refer==>",pic_refer[0]['href'])
                        #header['Referer'] = pic_refer[0]['href']

                        addRefer(pic_refer)

                        # 开始下载图片
                        html = requests.get(pic_url['src'], headers=header)
                        file_name = pic_url['src'].split(r'/')[-1]

                        print("header:", header)

                        try:
                            f = open(file_name, 'wb')
                            for data in html.iter_content():
                                f.write(data)
                            f.close()
                        except Exception as e:
                            print(title, "write error", end='\n')
                            print("\033[0;31m%s\033[0m" % e.args)
                        # f.write(html.content)
                    except Exception as e:
                        print(e)
                    except html.status_code == 200:
                        # 文件下载成功
                        print(file_name)
                        print("\033[0;31m%s\033[0m" % '下载成功')
                    except html.status_code == 403:
                        print("pic 403")

                print('完成')
        print('第', n, '页完成')



if __name__ == '__main__':
    """
        启动程序
    """
    main_scrap()
