# 为项目添加菜单界面
# 最简单的学生信息管理系统
#     +------------------------------+
#     | 1) 添加学生信息               |
#     | 2) 显示学生信息               |
#     | 3) 删除学生信息               |
#     | 4) 修改学生成绩               |
#     | 5) 按学生成绩高-低显示学生信息  |
#     | 6) 按学生成绩低-高显示学生信息  |
#     | 7) 按学生年龄高-低显示学生信息  |
#     | 8) 按学生年龄低-高显示学生信息  |
#     | q) 退出程序                   |
#     +------------------------------+

def input_student():
    '''
        此函数返回用户输入的学生信息的列表
        :return:
    '''

    L = [] #创建一个列表，用来保护所有学生的字典

    # 读取学生数据放入列表中
    while True:
        n = input("请输入学生姓名:")
        if n.strip() == "":
            print("--------输入结束---------")
            break
        #print("继续输入")
        a = input("请输入学生年龄:")
        s = input("请输入学生成绩:")
        # 把上述n,a,s 所表示的信息形成字典，并加入到列表中
        d = {} # 创建一个字符安，准备存入学生信息数据
        d['name'] = n
        d['age'] = a
        d['score'] = s
        L.append(d) # 把字典存入到列表中
    return L


def output_student(L):
    '''此函数传入的列表，打印成为表格'''
    # 第二步，显示所有学生的信息
    print("+-----------+------------+------------+")
    print("    name    |    age     |    score   |")
    print("+-----------+------------+------------+")

    #此处打印所有学生信息
    for d in L: # 把每个学生的数据取出来，用d绑定对应的字典
        center_name  = d['name'].center(11)
        center_age   = str(d['age']).center(12)
        center_score = str(d['score']).center(12)
        line = "|%s|%s|%s|" % (center_name,center_age,center_score)
        print(line)
    print("+-----------+------------+------------+")

def del_student(L): #删除学生信息
    name = input("请输入要修改的学生的姓名:")
    print("删除成功")

def modify_student_score(L):
    # 查找name 是不是在列表L里
    # 如果name在L里
    score = int(input("请输入要修改的成绩:"))
    # ...

def score_h_l(L):
    s = int(L['score'])
    return s

def age_h_l(L):
    print(L)
    a= L['score']
    return a

def show_menu():
    print("+-------------menu------------+")
    print("| 1) 添加学生信息               |")
    print("| 2) 显示学生信息               |")
    print("| 3) 删除学生信息               |")
    print("| 4) 修改学生成绩               |")
    print("| 5) 按学生成绩高-低显示学生信息  |")
    print("| 6) 按学生成绩低-高显示学生信息  |")
    print("| 7) 按学生年龄高-低显示学生信息  |")
    print("| 8) 按学生年龄低-高显示学生信息  |")
    print("| q) 退出程序                   |" )
    print("+------------------------------+")

def main():
    docs = []
    while True:
        show_menu()
        s = input("请选择：")
        if s == '1':
            docs += input_student()
        elif s == '2':
            output_student(docs)
        elif s == '3':
            del_student(docs) # 删除学生信息
        elif s == '4':
            modify_student_score(docs)
        elif s == '6':
            output_student(sorted(docs,key=score_h_l()))
        elif s == '5':
            output_student(sorted(docs,key=score_h_l(),reverse=True))
        elif s == '7':
            output_student(sorted(docs,key=age_h_l()))
        elif s == 'q':
            break # 退出循环


# 执行程序
main()













