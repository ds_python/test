from mysql_test import Mysqlpy
from hashlib import sha1

class CNNRegister:

    menu = "1.注册" \
           "2.登录" \
           "3.退出"

    def __init__(self):
        pass

    def cn_show_menu(self):
        while True:
            choice = input(CNNRegister.menu)
            if choice.strip() == "1":
                self.cn_register("test")
            elif choice.strip() == "2":
                self.cn_login()
            else:
                print("paragram exit!")
                break
            # pwd 这边没有加密   if len(result) == 0:

    def cn_login(self):
        uname = input("用户名:")
        password = input("密码:")
        sel = 'select password from user where username=%s'
        sqlObject = Mysqlpy('db5')
        result = sqlObject.fetchAll(sel, [uname])
        print("cn_login",result)

        pwd = "123456"
        s = sha1()
        s.update(pwd.encode("utf-8"))
        pwd = s.hexdigets()
        if len(result) == 0:
            print("用户名不存在")
        elif result[0][0] == pwd:
            print("登录成功")

    def cn_register(self, name=''):
        uname = name
        sel = 'select * from user where username=%s'
        sqlObject = Mysqlpy('db5')
        result = sqlObject.fetchAll(sel, [uname])

        if len(result) != 0:
            print("该用户已经存在")
        else:
            pwd1 = input("请输入密码:")
            pwd2 = input("请输入密码:")
            if pwd1 == pwd2:
                # 给密码进行加密,返回16进制加密结果
                s = sha1()
                s.update(pwd1.encode('utf-8'))  # sha1加密
                # pwd = s.hexdigets()
                # print("encode password:",pwd)
                ins = 'insert into user(username, password) value (%s,%s)'
                sqlObject.exe(ins, [uname,pwd1])
                print("恭喜您,注册成功!")
                pass
            else:
                print("两次密码不一致")

if __name__ == "__main__":
    registerObject = CNNRegister()
    registerObject.cn_register("libai")

    # mysql 优化
    # 1.选择的合适存储引擎
    # -1.1.读取操作多，用MyISAM,写操作多 InnoDB
    # 2.创建索引
    # 3.sql语句优化
    # -3.1 查询语句优化，为了避免全表扫描
    # a1:where 字句中，尽量不要使用 !=,否则放弃索引，全表扫描
    # a2:尽量避免 NULL 判断
    # 优化前：select num from t1 where num is null;
    # 优化后：select num from t1 where num=0; (创建表num设置默认值0)
    # a3:尽量使用 or 链接条件，否则....
    # 优化前: select id from t1 where id = 10 or id = 20;
    # 优化后: select id from t1 where id = 10 union all select id from t1 where id = 20;
    # a4:模糊查询，避免使用前置 %，否则...
    # select name from t1 where where name like %c%;
    # a5:尽量避免使用 in 和 not in
    # select id from t1 where id in (1,2,3,4)
    # select id from t1 where id  between 1 and 4;
    # a6:尽量不要使用 select *