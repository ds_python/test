# 信号量实现多进程锁
from multiprocessing import Process
from multiprocessing import Value
from multiprocessing import Semaphore
import os
import time

shm_v = Value('i', 10000)
alock = Semaphore()


def task_process(lock):
    for _ in range(1000000):
        # with alock:
        lock.acquire()
        v = shm_v.value  # 获取共享内存的值对象
        v += 1
        shm_v.value = v
        lock.release()
        # time.sleep(1)


p = Process(target=task_process, args=(alock,))
p.start()

for _ in range(1000000):
    with alock:
        v = shm_v.value
        v -= 1
        shm_v.value = v
    #time.sleep(1)



p.join()

print("主进程中获取到的值:", shm_v.value)

print("程序正常退出")



