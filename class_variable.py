class Human:
    total_count = 0  # 创建类变量

    def __init__(self, n):
        self.name = n

    def __del__(self):
        self.__class__.total_count -= 1  # 对象销毁时会-1

    @classmethod
    def get_count(cls):
        return cls.total_count

    @classmethod
    def set_count(cls,value):
        cls.total_count = value


h1 = Human("小张")  # 创建实例对象,有形参必须重写构造方法
h1.total_count = 100  # 为自己添加实例变量，并不修改类变量
print(h1.__class__.total_count)
h1.__class__.total_count = 200

print(h1.total_count)  # 只能获取数据，不能修改

print(Human.total_count)  # 可以修改数据

























