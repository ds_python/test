import json
import pymysql

# pymsql用法
print("hello word!")
# 写一个函数求n的阶乘


def mysum(n):
    if n == 1:
        return 1
    return n+mysum(n-1)


print(mysum(100))
# Python 字典类型转换为 JSON 对象
data = {
    'no': 1,
    'name': 'Runoob',
    'url': 'http://www.runoob.com'
}

json_str = json.dumps(data)
print("Python 原始数据：", repr(data))

# 转换成json对象
print("JSON 对象：", json_str)


def fetch_table_mysql():

    # 打开数据库连接
    db = pymysql.connect("localhost", "root",12345678,"360Mall")

    # 使用cursor()方法获取操作游标
    cursor = db.cursor()

    # SQL 查询语句
    sql = "SELECT * FROM EMPLOYEE \
           WHERE INCOME > '%d'" % (1000)
    try:
        # 执行SQL语句
        cursor.execute(sql)
        # 获取所有记录列表
        results = cursor.fetchall()
        for row in results:
            fname = row[0]
            lname = row[1]
            age = row[2]
            sex = row[3]
            income = row[4]
            # 打印结果
            print("fname=%s,lname=%s,age=%d,sex=%s,income=%d" % \
                  (fname, lname, age, sex, income))
    except:
        print("Error: unable to fetch data")

    # 关闭数据库连接
    db.close()


def update_table_mysql():

    # 打开数据库连接
    db = pymysql.connect("localhost", "root",12345678,"360Mall")

    # 使用cursor()方法获取操作游标
    cursor = db.cursor()

    # SQL 更新语句
    sql = "UPDATE EMPLOYEE SET AGE = AGE + 1 WHERE SEX = '%c'" % ('M')
    try:
        # 执行SQL语句
        cursor.execute(sql)
        # 提交到数据库执行
        db.commit()
    except:
        # 发生错误时回滚
        db.rollback()

    # 关闭数据库连接
    db.close()


def delete_table_mysql():
    """
        删除表中数据
    :return:
    """
    # 打开数据库连接
    db = pymysql.connect("localhost", "root","root","360Mall")
    # 使用cursor()方法获取操作游标
    cursor = db.cursor()

    # SQL 删除语句
    sql = "DELETE FROM EMPLOYEE WHERE AGE > '%d'" % (20)
    try:
        # 执行SQL语句
        cursor.execute(sql)
        # 提交修改
        db.commit()

    except:
        # 发生错误时回滚
        db.rollback()
    # 关闭连接
    db.close()


def insert_table_mysql():
    """
        EMPLOYEE 插入数据
    :return:
    """

    # 打开数据库连接
    db = pymysql.connect("localhost", "root",12345678,"360Mall")

    # 使用cursor()方法获取操作游标
    cursor = db.cursor()

    # SQL 插入语句
    sql = """INSERT INTO EMPLOYEE(FIRST_NAME,
             LAST_NAME, AGE, SEX, INCOME)
             VALUES ('Mac', 'Mohan', 20, 'M', 2000)"""
    try:
        # 执行sql语句
        cursor.execute(sql)
        # 提交到数据库执行
        db.commit()

    except:
        # 如果发生错误则回滚
        db.rollback()

    # 关闭数据库连接
    db.close()


def creat_table_mysql():

    # 打开数据库连接
    db = pymysql.connect("localhost", "root",12345678,"360Mall")

    # 使用 cursor() 方法创建一个游标对象 cursor
    cursor = db.cursor()

    # 使用 execute() 方法执行 SQL，如果表存在则删除
    cursor.execute("DROP TABLE IF EXISTS EMPLOYEE")

    # 使用预处理语句创建表
    sql = '''CREATE TABLE EMPLOYEE (
             FIRST_NAME  CHAR(20) NOT NULL,
             LAST_NAME  CHAR(20),
             AGE INT,  
             SEX CHAR(1),
             INCOME FLOAT )'''

    cursor.execute(sql)

    # 关闭数据库连接
    db.close()


def connect_mysql():

    # 打开数据库连接
    db = pymysql.connect("localhost", "root", "12345678", "360Mall")
    # 使用 cursor() 方法创建一个游标对象 cursor
    cursor = db.cursor()

    # 使用 execute()  方法执行 SQL 查询
    cursor.execute("SELECT VERSION()")

    # 使用 fetchone() 方法获取单条数据.
    data = cursor.fetchone()

    print("Database version : %s " % data)

    # 关闭数据库连接
    db.close()
    return "connect success!"


print(connect_mysql())
print(creat_table_mysql())
print(insert_table_mysql())




