import socket

class CNNUdpBroadcastRecv:
    def __init__(self):
        pass

    def connect(self):
        udp_recv = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
        udp_recv.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

        while True:
            try:
                recv_bytes, addr = udp_recv.recvfrom(1024)
                recv_str = recv_bytes.decode()  # 转为字符串
                print("接收到的ip地址为", addr[0], "的主机发出广播", recv_str)
            except InterruptedError as e:
                print(e)

        udp_recv.close()

if __name__ == "__main__":
    recv = CNNUdpBroadcastRecv()
    recv.connect()