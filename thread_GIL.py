
from threading import Thread
from multiprocessing import Process
import time


def task():
    for i in range(1000000000):
        pass


tasks = []

for _ in range(10):
    # t = Thread(target=task)
    p = Process(target=task)
    tasks.append(p)
    p.start()
    # time.sleep(1)

print("开始计算的时间:", time.time())
begin = time.time()
while tasks:
    tasks[0].join()
    del tasks[0]

print("程序结束", time.time())
print("总用时", time.time() - begin)
