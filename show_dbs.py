# 列出服务器上所有的库名称
import pymongo
from db_conf import *
from socket import *

from_img = "/Users/dushuzhong/Pictures/mac_bg.jpeg"
to_img = "/Users/dushuzhong/Pictures/mac_bg_new.jpeg"


class CNMongoDBBase:

    name = 'CNMongoDBBase'  # mongo 的基本操作

    def __init__(self, n=''):
        print("CNMongoBase init ...", n, )
        pass

    @staticmethod
    def cn_mongodb():
        conn = pymongo.MongoClient(host, port)
        db_list = conn.database_names()
        # print(db_list)
        for db_name in db_list:
            print(db_name)
        conn.close()

    @staticmethod
    def cn_insert():
        conn, db, coll = CNMongoDBBase.cn_bank_coll()
        my_dict = {
            "acct_no": "62223458888",
            "acct_name": "Jason",
            "balance": 3333.1,
            "acct_type": 2
        }
        ret = coll.insert_one(my_dict)  # 插入数据
        # ret = coll.save(my_dict)  #插入数据

        print("插入成功，产生的_id:", ret.inserted_id)

    @staticmethod
    def cn_update():
        conn, db, coll = CNMongoDBBase.cn_bank_coll()
        qry = {"acct_no": "62223458888"}
        new_value = {"$set": {"balance": 444}}
        ret = coll.update_one(qry, new_value, False, True)
        print("修改笔数:", ret.modified_count)

    @staticmethod
    def cn_bank_coll():
        conn = pymongo.MongoClient(host, port)
        db = conn["bank"]  # 获取数据库
        coll = db["acct"]  # 获取集合
        return conn, db, coll

    @staticmethod
    def cn_delete():
        conn, db, coll = CNMongoDBBase.cn_bank_coll()
        qry = {"acct_no": "62223458888"}
        ret = coll.delete_one(qry)
        print("删除笔数:", ret.deleted_count)

    @staticmethod
    def cn_query():
        conn, db, coll = CNMongoDBBase.cn_bank_coll()
        print("数据库：bank")
        qry = {}
        # qry = {"acct_name": "Jerry"}  #按名称查询
        # qry = {"acct_no": "6223450001", "acct_name": "Jerry"}  # 多个条件
        # qry = {"balance": {"$gt": 1000}}  # 余额大于5000
        # qry = {"$or": [
        #     {"acct_name": "Jerry"},
        #     {"acct_name": "Tom"}]
        # }  #或者查询
        prj = {"_id": False}  # 域限制
        result = coll.find(qry, prj)  # 执行查询
        # print(result)  # 得到可迭代对象
        for r in result:
            # print(r) #  没有解析r,单条记录
            CNMongoDBBase.cn_parse_dict(r)
        conn.close()

    @staticmethod
    def cn_parse_dict(r):
        """
        解析mongoDB中读取的集合数据
        :param r:
        :return:
        """
        try:
            acct_no = r["acct_no"]
            acct_name = r["acct_name"]
            balance = r["balance"]

            print("账号:%s, 户名:%s, 余额:%.2f" % (acct_no, acct_name, balance))

        except KeyError:  # as e
            # print("发送错误", e)
            pass

    def save_img(self, my_col):
        """
        存储数据到 mongo_db
        :param my_col: 集合
        :return:
        """
        f = open(from_img, "rb")
        data = f.read()  # 读取文件所有内容

        import bson.binary
        content = bson.binary.Binary(data)
        my_dict = {
            "filename": "mac_bg.jpeg",
            "data": content
        }
        my_col.insert(my_dict)  # 执行插入
        print("保存图片到mongodb成功！")

    def get_img_from_mongodb(self, my_col):
        """
        从mongodb集合中读取img
        :param my_col: 集合名称
        :return:
        """
        img = my_col.find_one({
            "filename": "mac_bg.jpeg",
        })
        print(img)
        with open(to_img, "wb") as f:
            f.write(img["data"])
        print("提取成功！")

    def cn_gridfs_coll(self):
        conn = pymongo.MongoClient(host, port)
        db = conn.gridfs
        my_col = db.image  # 获取集合对象
        # self.save_img(my_col)
        conn.close()
        return my_col


client = None
host = "127.0.0.1"
port = 9999  # 网络端口


class CNNAccountClient(CNMongoDBBase):
    def __init__(self):
        pass

    def base_config(self):
        # print("CNNAccountClient 基本信息:", CNNAccountClient.client, CNNAccountClient.host, CNNAccountClient.port)
        pass

    def show_menu(self):  # 打印菜单
        menu = '''
        -------------账号管理系统--------------
            1. 查询账号
            2. 新建账号
            3. 修改账号
            4. 删除账户
            5. 退出
        '''
        print(menu)

    def open_connection(self):
        """
            建立到服务器的链接
        :return:
        """
        try:
            global client
            client = socket()
            client.connect(host, port)
            print("连接服务器成功")
            return 0
        except ValueError:
            print("链接服务器失败")
            return -1

    def close_connection(self):
        if not client:  # client是空对象
            pass
        else:
            client.close()  # 关闭socket链接

    def client_query(self):
        """
            拼接一个字符串给服务端
        :return:
        """
        acct_no = input("请输入要查询的账号:")
        msg = ""
        if acct_no == "" and acct_no != "":  #用户输入了账号 if not acct_no
            msg = "query::all"  # 查询所有
            pass
        else:
            msg = "query::" + acct_no  #根据账号进行查询
        # 发送请求字符串
        if msg < 0:
            print("发送请求失败")
            return
        data = recv_msg()  # 接收服务器信息
        if not data:
            print("接收失败")
        else:
            print(data.decode())  #打印结果


    def send_message(self, msg):
        """
            发送数据
        :param msg:
        :return:
        """
        if not client or not msg:
            return -1
        else:
            n = client.send(msg.encode())
            return n  # 返回发送的字节数

    def recieve_message(self):
        global client
        if not client:
            return -1
        data = client.recv(10*1024)  # 一次最多接收10K
        return data

    def client_exec_selected(self):
        self.open_connection()

        while True:
            self.show_menu()
            operate_code = input("please select:")

            if not operate_code:  # 用户没有输入或者为空
                continue
            if operate_code == "1":  # 查询
                self.client_query()
                pass
            elif operate_code == "2":  #新增
                pass
            elif operate_code == "3":  #修改
                pass
            elif operate_code == "4":  # 删除
                pass
            elif operate_code == "5":  # 退出
                break
                pass
            else:
                print("输入非法")
        self.close_connection()  # 主程序退出前端口socket链接






# cn_mongodb()
# cn_insert()
# cn_update()
# cn_delete()
# cn_query()

if __name__ == "__main__":
    CNMongoDBBase.cn_query()

    cn_mongo_base = CNMongoDBBase('')
    # my_col = cn_mongo_base.cn_gridfs_coll()
    # cn_mongo_base.get_img_from_mongodb(my_col)

