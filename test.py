#
# # 字符串
# s = '姓名：%s,年龄：%d'
#
# # 循环语句
# # while:
# #     语句1
# # for
# #a = 1
# print("======== for 语句======== \n")
#
# for a in range(1,21):
#     print(a,end=' ')
#     #a += 1
# print("\n")


#
# print("======== while 语句====== \n")
# i = 1
# while(i <= 20):
#     print(i,end=' ')
#     if i% 5 == 0:
#         print("\n")
#     i += 1
# else:
#     print("循环结束\n")

# while 循环注意事项
# 1.控制真值表达式的值来防止死循环
# 2.通常真值表达式内的循环变量来控制循环条件
# 3.

# del 语句 删除变量
# pass 语句
#

#字符串处理 chr ord

# 类中实现,才能提供迭代器方法
# __iter__(self) 返回迭代器
# __next__ 实现迭代器协议


class Fibonacci:
    def __init__(self, n):
        self.count = n

    def __iter__(self):
        return FibonacciIter(self.count)


class FibonacciIter:
    def __init__(self, n):
        self.count = n  # 记录个数
        self.a = 0  # 记录第一个数
        self.b = 1  # 记录第二个数
        self.cur_count = 0

    def __next__(self):
        if self.cur_count >= self.count:
            # 说明已经生成完成,如果是异步的就会抛出异常
            raise StopIteration
        r = self.b
        self.a, self.b = self.b, self.a + self.b
        self.cur_count += 1
        return r


for x in Fibonacci(10):
    print('Fibonacci =====>', x)


print("======== 开始做练习题 ========")


def doWork1():
    begin = int(input("请输入开始值:"))
    end = int(input("请输入结束值:"))

    while(begin <= end):
        print(begin,end=' ')
        begin += 1
    else:
        print("\n循环结束\n",doWork1())


# (a-begin) % 5 == 4

def doWork2():
    # 统计个数
    count = 1
    begin = int(input("请输入开始值:"))
    end = int(input("请输入结束值:"))

    # 包含输入的结束值
    end += 1

    for a in range(begin,end):
        print(a,end=' ')
        # 方法1
        # if count % 5 == 0:
        #     print("\n")

        # 方法2
        if (a - begin) % 5 == 4:
            print("\n")
        a += 1
        count += 1

    print("\n===== 练习结束 ======\n")

    print("再来一遍====\n",doWork2())


def square():

    a = int(input("请输入一个整数:\n"))

    i = 1
    # 控制外层循环
    while(i <= a):
        j = 1
        while(j <= a):
            print(j,end='    ')
            j += 1
        else:
            print("\n")
        i += 1
    print("再次输入",square())

#doWork1()
#doWork2()


def sum():

    i = 0
    while 1:
        a = int(input("请输入整数:"))
        if a < 0:
            break
        i += a

    print("输入的数的和:",i)


def doWork3():
    s = 'ABCD'
    for c in s:
        #print(c,end=' ')
        if c == 'B':
            break
        print('chr---->',c)
    else:
        print("exit")

def doWork4():
    '''
        输出其中包含a的个数
    :return:
    '''
    s = input("请输入字符串:")
    count = 0
    for c in s:
        if c == 'a':
            count += 1

    print("字符串个数===>",count)

def doWork5():
    print("dowork5")
    for i in range(1,21):
        print(i,end=' ')

def doWork6():
    print("\ndowork6")
    for i in range(1,100):
        if (i*(i+1)) % 11 == 8:
            print(i,end=' ')
    print()

def doWork7():
    print("dowork7")
    sum = 0
    for i in range(1,100,2):
        #print(i)
        sum += i

    print(sum,end=' ')

def doWork8():
    i = 1
    sum = 0
    while i < 100:
        sum += i
        i += 2
    print("while求和：",sum)

def doWork9():
    '''
        实现一个三角形 4
        *
        **
        ***
        ****
    :return:
    '''
    print("")

def doWork10():
    '''
        *
       **
      ***
     ****
    :return:
    '''
    print("")


def doWork11():
    print("")


#只能被1和自身整除，且大于0
def doWork12():

    # x,2,3,4...x-1 整除，则不是素数
    print("")

#输入一个整数，按宽度打印如下矩形
# 5
# 1 2 3 4 5
# 2 3 4 5 6
# 3 4 5 6 7
# 4 5 6 7 8
# 5 6 7 8 9




#square()

#sum()

#doWork3()

#doWork4()

#doWork5()

#doWork6()

#doWork7()

#doWork8()