import socket
from netTest import CNNSockedServerManager

# udp 广播应用,一点发送多点接收，类似视频直播
# 让udp的套接字能发送和接收广播的属性
# upd_sock_setscokopt(level,optname,value)
# level: SOL_SOCED optname:SO_BROADCAST value: 1 可以发送和接收广播 0 禁止
#

class CNNUdpClient:
    def __init__(self):
        pass

    def notif(self):
        udp_client = socket.socket(socket.AF_INET,socket.SOCK_DGRAM,0)
        server_addr = ('192.168.43.30',8888)
        #udp_client.

    def connect(self):
        # 1.创建与服务端同样的套接字
        #socket.socket(socket.AF_INET,socket.SOCK_DGRAM,0)
        # 2.绑定（可选）
        # 3.发送和接收
        # socket.recvfrom 或者 socket.sendto
        # 关闭套接字，转成字节串
        udp_client = socket.socket(socket.AF_INET,socket.SOCK_DGRAM,0)
        server_addr = ('192.168.43.30',8888)
        while True:
            s = input("请输入要发送给服务器的数据:")
            send_bytes = s.encode()
            n = udp_client.sendto(send_bytes,server_addr)
            print("您已经成功发送", n, "字节")
            recv_bytes,addr = udp_client.recvfrom(1024)
            recv_str = recv_bytes.decode()
            print("收到来自服务器的数据",recv_str)
        udp_client.close()

    def test(self):
        # 创建一个socket对象
        obj = socket.socket()
        # 制定服务端的IP地址和端口
        obj.connect('127.0.0.1', 8080)
        # 阻塞，等待服务端发送内容，接受服务端发送过来的内容，最大接受1024字节
        ret_bytes = obj.recv(1024)
        # 因为服务端发送过来的是字节，所以我们需要把字节转换为字符串进行输出
        ret_str = str(ret_bytes, encoding="utf-8")
        # 输出内容
        print(ret_str)

        while True:
            # 当进入连接的时候，提示让用户输入内容
            inp = input("Client请输入要发送的内容>>> ")
            # 如果输出q则退出
            if inp == "q":
                # 把q发送给服务端
                obj.sendall(bytes(inp, encoding="utf-8"))
                # 退出当前while
                break
            else:
                # 否则就把用户输入的内容发送给用户
                obj.sendall(bytes(inp, encoding="utf-8"))
                # 等待服务端回答
                print("正在等待Server输入内容......")
                # 获取服务端发送过来的结果
                ret = str(obj.recv(1024), encoding="utf-8")
                # 输出结果
                print(ret)

        # 连接完成之后关闭链接
        obj.close()

if __name__ == "__main__":
    client = CNNUdpClient()
    client.connect()
